===================
 Meissner screening
===================
.. role:: bash(code)
   :language: bash

**Goal:** This guide shows how to study the diamagnetic screening of the Meissner state. In particular, the goal is to plot the spatial dependence of the screening currents and fields, for different values of the superconducting penetration depth. To do so, a disc-shaped superconductor with *s*-wave pairing symmetry is simulated, and the currents and fields are plotted radially along the disc. When the penetration depth is comparable or larger than the disc radius, the disc is largely unscreened, and the screening from each part of the edge highly overlaps at the center.

**Files:** The files used in this guide can be found in the folder :bash:`examples/parameter_sweeps/` in the |SuperConga repository|.

.. |SuperConga repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/tree/master/examples" target="_blank">SuperConga repository</a>

**Extensions:** This example can easily be extended to study the Meissner response in other superconducting orders (e.g. *d*-wave), or other interesting spatial dependencies.

----

Physics background: superflow response
======================================
Superconducting pairs are phase-coherent, with a phase :math:`\chi` which is directly related to superflows :math:`\mathbf{p}_{\mathrm{s}}` and the electromagnetic gauge :math:`\mathbf{A}`, via the Gauge invariant expression

.. math::
    :label: superflow

    \mathbf{p}_{\mathrm{s}}(\mathbf{R}) = \frac{\hbar}{2}\boldsymbol{\nabla}\chi(\mathbf{R}) - \frac{e}{c}\mathbf{A}(\mathbf{R}),
    
where :math:`e` is the elementary charge. Below, we consider negative charge, :math:`e = -|e|`, but note that SuperConga can simulate both positive and negative charge carriers, set via :bash:`"charge_sign"` in the config file, or the command-line argument :bash:`--charge-sign`. The superflow can be seen as the momentum of the superconducting pairs, and gives rise to a charge-current density :math:`\mathbf{j}`, via the response tensor :math:`\boldsymbol{\rho}`, i.e. the superfluid-.density tensor. This tensor is related to the superfluid stiffness and the superfluid weight. An overall positive or negative stiffness leads to an overall diamagnetic or paramagnetic response, respectively. In general, the response can be both non-local and non-linear, and should be properly derived e.g. from the quasiparticle propagators. The non-locality and non-linearity arise mainly when the system hosts significant spatial dependence or quasiparticles (e.g. nodal or thermal excitations). In the case of linear response, the relation between superflow and the current becomes

.. math::
    :label: supercurrent

    \mathbf{j}(\mathbf{R}) = \int d\mathbf{R}^{\prime} \boldsymbol{\rho}(\mathbf{R},\mathbf{R}^{\prime}) \cdot \mathbf{p}_{\mathrm{s}}(\mathbf{R}^{\prime}).
    
In a bulk *s*-wave superconductor at zero temperature, the supercurrent response is linear, local and diamagnetic :math:`\mathbf{j} = e\rho\mathbf{p}_{\mathrm{s}} = eN_{\mathrm{F}}\mathbf{v}_{\mathrm{F}}^2\mathbf{p}_{\mathrm{s}}`. We note that at finite temperatures, however, there is a finite paramagnetic contribution from thermally excited quasiparticle states which leads to non-linearities and a reduction of the overall diamagnetic response. The resulting depairing is also synonymous with a reduction in the superfluid density. A similar scenario arises in nodal superconductors (e.g. *d*-wave) even at zero temperature, due to nodal quasiparticles. :cite:p:`a-Xu:1995,a-Higashitani:1997`

It is the phase coherence of Eqs. :eq:`superflow` and :eq:`supercurrent` which gives rise to the Meissner effect, in which a superconductor becomes a perfect diamagnet in its bulk interior, expelling external fields :math:`\mathbf{B}_{\mathrm{ext}}`. This is achieved via screening currents at the interface of the superconductor, which give rise to an induced field :math:`\mathbf{B}_{\mathrm{ind}} = -\mathbf{B}_{\mathrm{ext}}` deep inside the superconductor. These screening currents and induction will now be simulated self-consistently with SuperConga.

Self-consistent simulations and results
=======================================
We set out to study an *s*-wave disc with radius :math:`\mathcal{R} = 15\xi_0`, exposed to an external magnetic flux of :math:`\Phi_{\mathrm{ext}} = 1.5\Phi_0`, where :math:`\xi_0 = \hbar v_{\mathrm{F}}/ 2\pi k_{\mathrm{B}}T_{\mathrm{c}}` is the superconducting coherence length and :math:`\Phi_0 \equiv hc/2|e|` is the magnetic-flux quantum. Furthermore, we want to vary the penetration depth :math:`\lambda`, ranging from :math:`\kappa \equiv \lambda/\xi_0 \in [1,\infty)`, i.e. in the whole Type-II range. This can be achieved by running the following script from the project root directory:

.. code-block:: bash
    :caption: Running the Meissner screening simulation.
    :name: meissner-screening-script-run

    ./examples/parameter_sweeps/swave_disc_meissner_kappa_sweep.sh

This will launch a number of simulations, each with a unique value of the penetration depth. The contents of the shell script file is:

.. code-block:: bash
    :caption: swave_disc_meissner_kappa_sweep.sh
    :name: meissner-screening-script
    
    #!/bin/bash

    # Note, this script is meant to be executed from the root folder of the project.
    # ./examples/parameter_sweeps/swave_disc_meissner_kappa_sweep.sh

    # The configuration file to use.
    CONFIG="examples/parameter_sweeps/kappa_sweep_config.json"

    # Base path to save the data to.
    DATA_PATH="data/examples/swave_disc_meissner_kappa_sweep"

    # The number of burn-in iterations.
    BURNIN=-1

    python superconga.py simulate -C $CONFIG -S $DATA_PATH/kInf --no-visualize -k -1
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k64 -L $DATA_PATH/kInf -b $BURNIN --no-visualize -k 64
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k32 -L $DATA_PATH/k64 -b $BURNIN --no-visualize -k 32
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k16 -L $DATA_PATH/k32 -b $BURNIN --no-visualize -k 16
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k8 -L $DATA_PATH/k16 -b $BURNIN --no-visualize -k 8
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k4 -L $DATA_PATH/k8 -b $BURNIN --no-visualize -k 4 
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k2 -L $DATA_PATH/k4 -b $BURNIN --no-visualize -k 2
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k1 -L $DATA_PATH/k2 -b $BURNIN --no-visualize -k 1

Here, the argument :bash:`-C|--config` specifies the path to the configuration file (the contents of which is shown below), the data output folder is specified by :bash:`-S|--save-path`, the Ginzburg-Landau coefficient is set with the argument :bash:`-k|--penetration-depth`, and it is specified that no live visualization shall be done during the simulations via the flag :bash:`--no-visualize`. The first simulation uses a bulk start guess, while all subsequent simulations use the previous one as a start guess. The burn-in is set to -1, indicating that the coherence functions at the boundary will be updated to self-consistently upon loading data from a previous simulation, before starting the new simulation. See the `tutorials`_ for more in-depth information about simulation settings.

The contents of the configuration file is:

.. code-block:: bash
   :caption: examples/parameter_sweeps/kappa_sweep_config.json
   :name: config-file-meissner-screening

    {
        "physics": {
            "temperature": 0.5,
            "external_flux_quanta": 1.5,
            "penetration_depth": 5.0,
            "crystal_axes_rotation": 0.0,
            "gauge": "symmetric",
            "charge_sign": -1,
            "order_parameter": {
                "s": {
                    "critical_temperature": 1.0,
                    "initial_phase_shift": 0.0,
                    "initial_noise_stddev": 0.0,
                    "vortices": []
                }
            }
        },
        "geometry": [
            {
                "disc": {
                    "add": true,
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "radius": 15.0
                }
            }
        ],
        "numerics": {
            "convergence_criterion": 1e-05,
            "energy_cutoff": 16.0,
            "norm": "l2",
            "num_fermi_momenta": 32,
            "num_iterations_burnin": 0,
            "num_iterations_max": 10000,
            "num_iterations_min": 0,
            "points_per_coherence_length": 20.0,
            "vector_potential_error": 1e-06
        },
        "accelerator": {
            "name": "barzilai-borwein"
        },
        "misc": {
            "data_format": "h5",
            "load_path": "",
            "save_frequency": -1,
            "save_path": "data/examples/swave_disc_vortex_kappa_sweep",
            "verbose": true,
            "visualize": true
        }
    }

After running the shell-scripts above, the results can be plotted by running the following Python script from the root directory:

.. code-block:: bash
   :caption: Generating the Meissner screening plot.
   :name: plotting-meissner-screening

   python examples/parameter_sweeps/plot_meissner_kappa_sweep.py

which should generate the following figure:

.. figure:: /_static/images/guides/meissner_screening.png

    Figure: Meissner screening in an *s*-wave superconducting disc of radius of :math:`\mathcal{R} = 15 \xi_0` at :math:`T=0.5T_{\mathrm{c}}`, exposed to an external magnetic-flux density of :math:`B_{\mathrm{ext}} = 1.5 \Phi_0/\mathcal{A}`, where :math:`\mathcal{A}` is the disc area. (a) Magnitude of the current density, with graphics indicating the direction of the external and induced fields, the direction of the currents, and the value of the Ginzburg-Landau coefficient. Panels (b) and (c) show the total current density and magnetic-flux density, respectively, along the horizontal dashed line in (a). Different lines correspond to different values of :math:`\kappa`, as indicated by the legend.

In the figure, different lines correspond to different values of the dimensionless Ginzburg-Landau coefficient :math:`\kappa = \lambda/\xi_0`, which is used to self-consistently solve the magnetic gauge field. It is seen that there are three different regimes, depending on the ratio of :math:`\lambda` to :math:`\mathcal{R}`:

1. :math:`\lambda \ll \mathcal{R}`: the interior of the superconductor is fully screened (:math:`B_{\mathrm{ext}} + B_{\mathrm{ind}} = 0`),
2. :math:`\lambda \lesssim \mathcal{R}`: partially screened,
3. :math:`\lambda \gtrsim \mathcal{R}` un-screened.

Higher fields
=============
The applied magnetic-flux density in the figure above is smaller than :math:`B_{\mathrm{c}} \sim \Phi_0/\xi_0\lambda`, which is the thermodynamic critical field where superconductivity is destroyed in a Type-I superconductor (the latter defined by :math:`\kappa < 1/\sqrt{2}`). It is also smaller than :math:`B_{\mathrm{c_1}} \sim \Phi_0/\lambda^2`, which is the first critical field of a Type-II superconductor (the latter defined by :math:`\kappa > 1/\sqrt{2}`). Please note that both of these critical fields in reality depend also on the size and geometry of the grain, see the SuperConga v1.0 paper. At the transition :math:`B_{\mathrm{ext}} = B_{\mathrm{c_1}}`, the free energy of a conventional bulk superconductor is minimized by the appearance of a triangular lattice of Abrikosov vortices. Abrikosov vortices are topological defects, with a quantized phase winding and a singular phase. In the above simulations, there is no net phase winding going around the disc (the superflow is carried by oscillating gradients and vector potential around the disc). The following guides are devoted to studying various properties of Abrikosov vortices:

- `a single Abrikosov vortex`_,
- `interactive vortex spectroscopy`_,
- `mesoscopic vortex lattices`_.

.. _`a single Abrikosov vortex`: abrikosov-vortex-core.html
.. _`interactive vortex spectroscopy`: vortex-core-spectroscopy.html
.. _`mesoscopic vortex lattices`: mesoscopic-vortex-lattices.html


.. _`tutorials`: ../tutorials.html

----

References
==========

.. bibliography::
    :labelprefix: A
    :keyprefix: a-
    :style: plain
