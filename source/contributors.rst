=========================
 Authors and contributors
=========================

SuperConga development is supported by the Applied Quantum Physics Laboratory at the Department of Microtechnology and Nanoscience - MC2, Chalmers University of Technology.

.. |_| unicode:: 0xA0 
   :trim:

Current developers
------------------
.. grid:: 2 3 3 3
    :gutter: 2

    .. grid-item-card:: 

        .. figure:: _static/images/developers/niclas_wall_wennerdal.png
            :width: 100%
            :align: center
        
        **Niclas Wall-Wennerdal**
        +++
        |niclas_contact| |_| |_| \| |_| |_| |niclas_website|

    .. grid-item-card::

        .. figure:: _static/images/developers/patric_holmvall.png
            :width: 100%
            :align: center
        
        **Patric Holmvall**
        +++
        |patric_contact| |_| |_| \| |_| |_| |patric_website|

    .. grid-item-card::

        .. figure:: _static/images/developers/pascal_stadler.png
            :width: 100%
            :align: center
        
        **Pascal Stadler**
        +++
        |pascal_website|


.. |niclas_website| raw:: html

   <a href="https://gitlab.com/niclas_wall_wennerdal" target="_blank" class="card-link">GitLab</a> 

.. |niclas_contact| raw:: html

   <a href="https://www.chalmers.se/en/persons/wennerda/" target="_blank" class="card-link">Contact</a> 

.. |patric_website| raw:: html

   <a href="https://gitlab.com/Holmvall" target="_blank" class="card-link">GitLab</a> 

.. |patric_contact| raw:: html

   <a href="https://katalog.uu.se/empinfo/?id=N20-2205" target="_blank" class="card-link">Contact</a> 

.. |pascal_website| raw:: html

   <a href="https://gitlab.com/pascalsta" target="_blank" class="card-link">GitLab</a> 


Project supervisors
-------------------
.. grid:: 2 3 3 3
    :gutter: 2

    .. grid-item-card:: 

        .. figure:: _static/images/developers/mikael_fogelstrom.png
            :width: 100%
            :align: center
        
        **Mikael Fogelström**
        +++
        |micke_contact| |_| |_| \| |_| |_| |micke_website|

    .. grid-item-card::

        .. figure:: _static/images/developers/tomas_lofwander.png
            :width: 100%
            :align: center
        
        **Tomas Löfwander**
        +++
        |tomas_contact| |_| |_| \| |_| |_| |tomas_website|

.. |micke_website| raw:: html

   <a href="https://scholar.google.com/citations?user=_D6EuMUAAAAJ&hl=en" target="_blank" class="card-link">Google Scholar</a> 

.. |micke_contact| raw:: html

   <a href="https://www.chalmers.se/en/persons/fogelstr/" target="_blank" class="card-link">Contact</a> 

.. |tomas_website| raw:: html

   <a href="https://scholar.google.se/citations?user=ISDRjEAAAAAJ&hl=en" target="_blank" class="card-link">Google Scholar</a> 

.. |tomas_contact| raw:: html

   <a href="https://www.chalmers.se/en/persons/lofwande/" target="_blank" class="card-link">Contact</a> 


Original/past developers
------------------------
.. grid:: 2 3 3 3
    :gutter: 2

    .. grid-item-card:: 

        .. figure:: _static/images/developers/mikael_hakansson.png
            :width: 100%
            :align: center
        
        **Mikael Håkansson**
        +++
        |mikael_website|

    .. grid-item-card::

        .. figure:: _static/images/developers/oleksii_shevtsov.png
            :width: 100%
            :align: center
        
        **Oleksii Shevtsov**
        +++
        |oleksii_website|


.. |mikael_website| raw:: html

   <a href="https://www.linkedin.com/in/mikael-h%C3%A5kansson-07549a1/" target="_blank" class="card-link">LinkedIn</a> 

.. |oleksii_website| raw:: html

   <a href="https://se.linkedin.com/in/oleksii-shevtsov" target="_blank" class="card-link">LinkedIn</a> 

----

Other Contributors
------------------
We encourage any useful contributions to the development of SuperConga, please see the `contribution guide`_ if interested! Below follows an alphabetical list of people and organizations who have contributed in one way or another.

.. _`contribution guide`: contribute.html

- **Anton B. Vorontsov**: valuable discussions.
- **Joe Jordan** (PDC): AMD GPU workshop mentoring, and help with porting the code for HIP/ROCm.
- **Kevin Marc Seija**: valuable discussions.
- |CodeRefinery|: education and workshops.
- |ENCCS|: education and workshops.
- |Nordic-RSE|: technical support community.


.. |ENCCS| raw:: html

   <a href="https://enccs.se/" target="_blank" class="card-link">ENCCS</a> 

.. |CodeRefinery| raw:: html

   <a href="https://coderefinery.org/" target="_blank" class="card-link">CodeRefinery</a>

.. |Nordic-RSE| raw:: html

   <a href="https://nordic-rse.org/" target="_blank" class="card-link">Nordic-RSE</a> 


----


Financial Support
-----------------

.. include:: funding_support.rst
