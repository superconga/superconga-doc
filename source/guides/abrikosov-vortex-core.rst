======================
 Abrikosov-vortex core
======================
.. role:: bash(code)
   :language: bash

**Goal:** This guide shows how to study the spatial dependence of the order parameter, charge-current density, and magnetic fields around an Abrikosov vortex, for different values of the penetration depth. To do so, a disc-shaped superconductor with *s*-wave pairing symmetry is simulated, with an order-parameter phase winding as a start guess. A vortex is stabilized at the center of the disc, with a paramagnetic core, surrounded by diamagnetic Meissner screening currents. The larger the penetration depth is, the less pronounced the diamagnetic screening is, due to an overlap between the edge screening currents and the vortex.

**Files:** The files used in this guide can be found in the folder :bash:`examples/parameter_sweeps` in the |SuperConga repository|.

.. |SuperConga repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/tree/master/examples" target="_blank">SuperConga repository</a>

----


Physics background: Abrikosov vortices
======================================
The applied magnetic-flux density in the `Meissner screening guide`_ is smaller than :math:`B_{\mathrm{c}} \sim \Phi_0/\xi_0\lambda`, which is the thermodynamic critical field where superconductivity is destroyed in a Type-I superconductor (defined by :math:`\kappa < 1/\sqrt{2}`), as well as :math:`B_{\mathrm{c_1}} \sim \Phi_0/\lambda^2`, which is the first critical field of a Type-II superconductor (defined by :math:`\kappa > 1/\sqrt{2}`) :cite:p:`c-Tinkham:2004`. Here, :math:`\xi_0 = \hbar v_{\mathrm{F}}/ 2\pi k_{\mathrm{B}}T_{\mathrm{c}}` is the superconducting coherence length and :math:`\Phi_0 \equiv hc/2|e|` the magnetic-flux quantum, :math:`\lambda` the penetration depth, and :math:`\kappa = \lambda/\xi_0` the Ginzburg-Landau coefficient. Above the transition :math:`B_{\mathrm{ext}} = B_{\mathrm{c_1}}`, the free energy of a conventional bulk superconductor is minimized by the appearance of a triangular lattice of Abrikosov vortices, where neighboring vortices are separated by a distance :math:`\sim \lambda`. As :math:`B_{\mathrm{ext}} \to B_{\mathrm{c_2}} \sim \Phi_0/\xi_0^2`, the vortices are separated by a distance :math:`\sim \xi_0`, and bulk superconductivity is lost. In a finite grain of size :math:`\mathcal{L}`, the vortex-physics is highly modified, see in particular the guide on `mesoscopic vortex lattices`_. For :math:`\lambda \ll \mathcal{L}`, the core is generally surrounded by usual Meissner screening currents, and the lattice becomes more bulk-like.

In this guide, we study an individual isolated vortex. Abrikosov vortices are topological defects characterized by integer phase winding and flux quantization, which locally suppress superconductivity via a normal core of radius :math:`\sim \xi_0`. Furthermore, the core hosts a huge density of midgap states.

.. _`mesoscopic vortex lattices`: mesoscopic-vortex-lattices.html


Self-consistent simulations and results
=======================================
The simulations are run from the project root directory via:

.. code-block:: bash
    :caption: Running the Abrikosov-vortex core simulation.
    :name: abrikosov-vortex-script-run

    ./examples/parameter_sweeps/swave_disc_vortex_kappa_sweep.sh

This will run multiple simulations of an *s*-wave disc with diameter :math:`\mathcal{R}=15\xi_0` exposed to an external flux :math:`\Phi_{\mathrm{ext}} = 1.5\Phi_0`, where each simulation has a unique value of the penetration depth. This flux is still below the first critical field, making the Abrikosov vortex metastable. It is very robust however, and cannot easily exit the system due to the Bean-Livingston barrier. **Note:** these simulations might take quite long, due to the high spatial resolution used (necessary for later calculating LDOS). The contents of the shell-script file is:

.. code-block:: bash
    :caption: swave_disc_vortex_kappa_sweep.sh
    :name: vortex-core-script
    
    #!/bin/bash

    # Note, this script is meant to be executed from the root folder of the project.
    # ./examples/parameter_sweeps/swave_disc_vortex_kappa_sweep.sh

    # The configuration file to use.
    CONFIG="examples/parameter_sweeps/kappa_sweep_config.json"

    # Base path to save the data to.
    DATA_PATH="data/examples/swave_disc_vortex_kappa_sweep"

    # The number of burn-in iterations.
    BURNIN=-1

    python superconga.py simulate -C $CONFIG -S $DATA_PATH/kInf --no-visualize -k -1 --vortex 0 0 -1
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k64 -L $DATA_PATH/kInf -b $BURNIN --no-visualize -k 64
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k32 -L $DATA_PATH/k64 -b $BURNIN --no-visualize -k 32
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k16 -L $DATA_PATH/k32 -b $BURNIN --no-visualize -k 16
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k8 -L $DATA_PATH/k16 -b $BURNIN --no-visualize -k 8
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k4 -L $DATA_PATH/k8 -b $BURNIN --no-visualize -k 4 
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k2 -L $DATA_PATH/k4 -b $BURNIN --no-visualize -k 2
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k1 -L $DATA_PATH/k2 -b $BURNIN --no-visualize -k 1


Here, the argument :bash:`-C|--config` specifies the path to the configuration file (the contents of which are shown below), the data output folder is specified by :bash:`-S|--save-path`, the Ginzburg-Landau coefficient is set with the argument :bash:`-k|--penetration-depth`, and it is specified that no live visualization shall be done during the simulations via the flag :bash:`--no-visualize`. The first simulation uses a start guess with a vortex (phase winding :math:`-2\pi`) at :math:`(x,y) = (0,0)\xi_0`, set via :bash:`--vortex 0 0 -1`. Subsequent simulations use data from the previous simulations as a start guess, with a burn-in. See the `tutorials`_ for more in-depth information about simulation settings.

The contents of the configuration file is:

.. code-block:: bash
   :caption: examples/parameter_sweeps/kappa_sweep_config.json
   :name: config-file-vortex-core

    {
        "physics": {
            "temperature": 0.5,
            "external_flux_quanta": 1.5,
            "penetration_depth": 5.0,
            "crystal_axes_rotation": 0.0,
            "gauge": "symmetric",
            "charge_sign": -1,
            "order_parameter": {
                "s": {
                    "critical_temperature": 1.0,
                    "initial_phase_shift": 0.0,
                    "initial_noise_stddev": 0.0,
                    "vortices": []
                }
            }
        },
        "geometry": [
            {
                "disc": {
                    "add": true,
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "radius": 15.0
                }
            }
        ],
        "numerics": {
            "convergence_criterion": 1e-05,
            "energy_cutoff": 16.0,
            "norm": "l2",
            "num_fermi_momenta": 32,
            "num_iterations_burnin": 0,
            "num_iterations_max": 10000,
            "num_iterations_min": 0,
            "points_per_coherence_length": 20.0,
            "vector_potential_error": 1e-06
        },
        "accelerator": {
            "name": "barzilai-borwein"
        },
        "misc": {
            "data_format": "h5",
            "load_path": "",
            "save_frequency": -1,
            "save_path": "data/examples/swave_disc_vortex_kappa_sweep",
            "verbose": true,
            "visualize": true
        }
    }

After running the simulations and producing the data with the shell-scripts above, the following Python script can be run (from the root directory) to generate the plot:

.. code-block:: bash
   :caption: Generating the Abrikosov-vortex core plot.
   :name: plotting-abrikosov-vortex

   python examples/parameter_sweeps/plot_vortex_kappa_sweep.py

which should generate the following figure:

.. figure:: /_static/images/guides/abrikosov_vortex_core.png

    Figure: A single Abrikosov vortex in an *s*-wave superconducting disc of radius :math:`\mathcal{R} = 15 \xi_0` at :math:`T=0.5T_{\mathrm{c}}`, exposed to an external magnetic-flux density of :math:`B_{\mathrm{ext}} = 1.5\Phi_0/\mathcal{A}`, where :math:`\mathcal{A}` is the disc area. (a) Magnitude of the current density, with graphics indicating the value of the Ginzburg-Landau coefficient, the direction of the external and induced fields, and the direction of the currents. The latter illustrates that the central region is paramagnetic, while the outer region is diamagnetic. Panel (b) shows the :math:`y`-component of the current density, while (c) shows the total magnetic-flux density, both along the horizontal dashed line in (a). The inset in (b) shows that the order-parameter phase winds :math:`-2\pi` around the center of the disc. Different lines correspond to different values of :math:`\kappa`, as indicated by the legend. Panel (d) shows a zoom of (c), illustrating that if :math:`\lambda \ll \mathcal{R}`, there is a region of finite diamagnetic screening around the vortex core. Increasing the external flux, this screening is increased, until another vortex enters the system.

Note that in panel (a), the magnitude is indicated by colors from two concatenated colormaps. The first one ends at 0.02, and is the same scale/color as in the Meissner example, to make it easier to compare the two figures. This is the diamagnetic region. The higher magnitude above 0.02 is captured by another colormap, to illustrate the paramagnetic region.

In general, we note that the region of diamagnetic screening is small, due to the relatively large ratio :math:`\lambda/\mathcal{R}`. As this ratio decreases, the region of full screening increases, in which :math:`B_{\mathrm{ext}} + B_{\mathrm{ind}} \to 0`.

See the following guide for more on Abrikosov vortices:

- `interactive vortex spectroscopy`_,
- `mesoscopic vortex lattices`_.

.. _`interactive vortex spectroscopy`: vortex-core-spectroscopy.html
.. _`mesoscopic vortex lattices`: mesoscopic-vortex-lattices.html

.. _`Meissner screening guide`: meissner-screening.html

.. _`tutorials`: ../tutorials.html

----

References
==========

.. bibliography::
    :labelprefix: C
    :keyprefix: c-
    :style: plain
