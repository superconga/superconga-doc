# SuperConga-doc

Link to documentation: https://superconga.gitlab.io/superconga-doc/

This repository contains the source files for the [SuperConga](https://gitlab.com/superconga/superconga) documentation.


## Build status

![Build Status](https://gitlab.com/superconga/superconga-doc/badges/master/pipeline.svg)


## GitLab CI

This project's static Pages are built by GitLab CI, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3.11

pages:
  stage: deploy
  script:
    - python3 -m venv venv
    - source venv/bin/activate
    - pip install -U pip
    - pip install -r requirements.txt
    - sphinx-build -b html source public
  artifacts:
    paths:
      - public
  only:
    - master
```

## Requirements
The requirements are handled explicitly by `pip` via [`requirements.txt`](requirements.txt)

- [Sphinx](https://www.sphinx-doc.org)
- [Read The Docs Sphinx Theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/)

## How to use locally
In order to run and preview locally, clone the repo. To compile and see the changes, follow the steps below (you need to create a virtual environment only once). This can be useful to preview changes before pushing to/merging in the repository, or to build and inspect an older version of the documentation (i.e. after checking out an old commit).

Create a Python 3 virtual environment using the venv module included with Python 3
```
python3 -m venv venv
```
Now “activate” the environment. Look for the name of the virtual environment enclosed in parenthesis after activation
```
source venv/bin/activate
```
Upgrade `pip` and install the necessary requirements
```
pip install -U pip
pip install -r requirements.txt
```
In order to preview changes, run the following command from the root folder
```
make html
```
This creates a new directory structure `public/html/` which contains the root of the documentation. Open it with the browser of your choice, e.g.
```
firefox public/html/index.html &
```
Upon adding new changes and rebuilding the documentation, refresh the browser tab to see the changes.
