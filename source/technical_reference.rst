=========================
 Technical reference
=========================
.. role:: bash(code)
   :language: bash

On this page, we will add detailed technical specifications, for example of the configuration files and command-line options. Currently, it only consists of an explanation of the postprocessing config file. In the meantime, we refer to the appendix of the manuscript for SuperConga v1.0, and the SuperConga help message.

----

Simulation configuration file
=============================
For detailed information about the simulation config files (i.e. :bash:`examples/<example_name>/simulation_config.json`), please refer to the appendix of the SuperConga v1.0 manuscript.

----

Command-line arguments
======================
For detailed information about command-line options, see :bash:`python superconga.py <subcommand> --help`, where :bash:`<subcommand>` is one of the valid SuperConga subcommands/tools (e.g. :bash:`simulate` and :bash:`postprocess`). For an overview of all possible subcommands, see :bash:`python superconga.py --help`.

----

Postprocess configuration
=========================
The numeric simulations that self-consistently compute the order parameter and vector potential are done using the equilibrium Matsubara propagators, summing energies (poles) on the imaginary axis. In contrast, the density of states is calculated on the real axis using the causal in time (Retarded) propagators. The DOS calculation is quite sensitive, generally requiring a much higher spatial resolution, number of Fermi momenta, and number of real energies. This makes the DOS and LDOS calculation expensive, which is why it is made as a postprocessing step. This is done by the backend :bash:`src/postprocess.cu`, which is called via the frontend through :bash:`python superconga.py postprocess`. See the corresponding help message for command-line options.

The most convenient way to run postprocessing is to supply a :bash:`.json`-file containing all the necessary simulation parameters. Each example in :bash:`examples/` has such a file, called :bash:`postprocess_config.json`, containing the following parameters.

The following shows the contents of the file :bash:`examples/swave_disc_vortex/postprocess.json`:

.. code-block:: bash
   :caption: **examples/swave_disc_vortex/postprocess_config.json**
   :name: reference-ldos-postprocess-json

    {
        "spectroscopy": {
            "energy_max": 0.5,
            "energy_min": 0.0,
            "energy_broadening": 0.008,
            "num_energies": 128
        },
        "numerics": {
            "convergence_criterion": 1e-4,
            "norm": "l2",
            "num_energies_per_block": 32,
            "num_fermi_momenta": 720,
            "num_iterations_burnin": -1,
            "num_iterations_max": 1000,
            "num_iterations_min": 0
        },
        "misc": {
            "data_format": "h5",
            "load_path": "data/examples/dwave_chiral",
            "save_path": "",
            "verbose": true
        }
    }

The parameters are split into the following groups:

- **spectroscopy:** parameters related to the energy, e.g. resolution and range.

    - **energy_max:** The maximum energy to compute the DOS at.
    - **energy_min:** The minimum energy to compute the DOS at. Note that it is usually enough to compute the DOS in a positive interval, and get the DOS at negative energies by symmetry.
    - **energy_broadening:** The finite energy broadening (imaginary part) added to the energy in the Retarded propagator.
    - **num_energies:** Number of energies in the interval :bash:`[energy_min, energy_max]`.

- **numerics:** parameters related to the numerics, e.g. accuracy and iterations.
    - **convergence_criterion:** Convergence criterion for self-consistency.
    - **norm:** The :math:`p`-norm to use when computing the residuals, with :math:`p \in \{1,2,\infty \}`.
    - **num_energies_per_block:** Number of energies to treat at a time (in parallel). DOS calculations are usually very memory-intensive, and one can easily run out of GPU memory. Reducing the number of energies per block can therefore alleviate this.
    - **num_fermi_momenta:** The number of discrete momenta to use when approximating the Fermi-surface averages :math:`\langle \ldots \rangle_{\mathbf{p}_{\mathrm{F}}}`.
    - **num_iterations_burnin:** Number of burn-in iterations to run, before starting the main simulation. During the burn-in, the order parameter and vector potential are kept fixed (since they are usually already self-consistent when loaded from file), while the coherence functions are updated (since they are NOT stored to file, due to being disk-space intensive). A negative value means that burn-in is run until the coherence functions (at the boundary) reach the self-consistency criterion.
    - **num_iterations_max:** Maximum number of iterations to run. Useful to avoid endless simulations.
    - **num_iterations_min:** Minimum number of self-consistent iterations to run. Useful to avoid terminating a simulation too early, i.e. when it is moving slowly on a plateau in phase-space.

- **misc:** other parameters, e.g. related to input/output data.
    - **data_format:** The data output format, e.g. :bash:`"h5"` or :bash:`"csv"`. The former is compressed, and therefore highly recommended due to the large data volumes.
    - **load_path:** The base folder where the order-parameter/vector-potential data is located. I.e. the output folder that was used when running :bash:`python superconga.py simulate`.
    - **save_path:** Base folder for input/output (data and parameter files).
    - **verbose:** If ``"verbose": true``, print simulation information to the terminal.
