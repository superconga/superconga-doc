================
 Citation
================

Have you used SuperConga, or in general find it useful? Please help us make SuperConga better and cite the publications and source code:

* P\. Holmvall, N. Wall-Wennerdal, M. Håkansson, P. Stadler, O. Shevtsov, T. Löfwander, and M. Fogelström, "|superconga_2023|", Applied Physics Reviews 10, 011317 (2023).


.. |superconga_2023| raw:: html

    <a href="https://doi.org/10.1063/5.0100324" target="_blank">SuperConga: An open-source framework for mesoscopic superconductivity</a>

.. |SuperConga source code| raw:: html

   <a href="" target="_blank">SuperConga: source code</a>

.. |SuperConga documentation| raw:: html

   <a href="" target="_blank">SuperConga: documentation</a>

In turn, please let us know if you have used SuperConga in your research, so that we can help increase the exposure of this research, e.g. in the `list of research using SuperConga`_ and in any of our presentations and outreach. We find any application of the SuperConga framework of great interest.

.. _`list of research using SuperConga`: about.html#research-using-superconga
