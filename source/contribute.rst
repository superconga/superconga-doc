===================
 Contribution guide
===================
.. role:: bash(code)
   :language: bash

We welcome any and all contributions that improve SuperConga, as long as they follow the |code of conduct|. Below follows a guide on how to contribute to the SuperConga project. In short, the guide describes how proposed contributions are made locally through forking and cloning, and proposed via a merge request. A crucial step is following the same source code style guides as in the SuperConga repository.

Please note that a basic working knowledge of git and GitLab is assumed. There are countless sources to learn more about these tools online. A good place to start is the |gitlab_documentation|, and in particular the parts about |cloning| and |forking| a repository. See the |coderefinery_material| for a great course on topics ranging from git version control to sustainable and open research code.

.. |code of conduct| raw:: html

    <a href="https://gitlab.com/superconga/superconga/-/blob/master/CODE_OF_CONDUCT.md" target="_blank">code of conduct</a>

.. |gitlab_documentation| raw:: html

    <a href="https://docs.gitlab.com/ee/README.html" target="_blank">GitLab documentation</a>

.. |coderefinery_material| raw:: html

   <a href="https://coderefinery.org/lessons/" target="_blank">CodeRefinery lessons material</a>

.. |cloning| raw:: html
   
      <a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository" target="_blank">cloning</a>

.. |forking| raw:: html

   <a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html" target="_blank">forking</a>

.. note::
    A |clone| is a local copy of the *files* in a repository, on which changes can be made. The changes can then be pushed to the centralized online (remote) repository. The latter requires that the user has the proper privileges in the online repository, but this is usually only reserved to the core team members of a repository. This is where forking comes in.
    
    A |fork| is a copy of the *entire repository*, for example useful to give a user full privileges, and to safely make changes externally from the centralized repository. The fork can then be cloned to make a local copy of the files, on which changes can be made. Even if the user does not have full privileges on the main repository, they can propose changes via the fork. Specifically, this is done via a merge request from the fork to the main repository.
   
Forking and cloning a repository
================================
The SuperConga project currently consists of several repositories (repos), that can be found at |main repo|. For example, there is one repository for the |SuperConga source code|, and one for generating the |online user manual|. A separate fork should be created for the repository that you wish to contribute to. Below, we use the source code repository as an example, but it follows equivalently for any other repository.

.. |main repo| raw:: html
   
      <a href="https://gitlab.com/superconga" target="_blank">https://gitlab.com/superconga</a>

.. |SuperConga source code| raw:: html
   
      <a href="https://gitlab.com/superconga/superconga" target="_blank">SuperConga source code</a>

.. |online user manual| raw:: html
   
      <a href="https://gitlab.com/superconga/superconga-doc" target="_blank">online user manual</a>

.. |clone| raw:: html
   
      <a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository" target="_blank">clone</a>

.. |fork| raw:: html

    <a href="https://docs.gitlab.com/ee/gitlab-basics/fork-project.html" target="_blank">fork</a>

Forking can be done in the top right corner of the main page a repository. It should take you to a page similar to: |SuperConga fork|

.. |SuperConga fork| raw:: html

    <a href="https://gitlab.com/superconga/superconga/-/forks/new" target="_blank">https://gitlab.com/superconga/superconga/-/forks/new</a>

After forking the repository, a local copy (that can actually be edited) is created via git clone from the command line, either via SSH

.. code-block:: bash
   :caption: **git clone: creating a local copy of a repository (SSH)**
   :name: contribute-git-clone

   git clone git@gitlab.com:<gitlabusername>/superconga.git

or HTTPS:

.. code-block:: bash
   :caption: **git clone: creating a local copy of a repository (HTTPS)**
   :name: contribute-git-clone-https

   git clone https://gitlab.com/<gitlabusername>/superconga.git

where :bash:`<gitlabusername>` should be replaced with the account/page hosting the fork. After this has been done, the parent repository has to be added as a remote, by running the following in the directory of the repo (the same which was created by the git clone command):

.. code-block:: bash
   :caption: **git remote: adding upstream parent**
   :name: contribute-git-remote

   git remote add upstream https://gitlab.com/superconga/superconga

Note that it is possible to add multiple remotes, e.g. to other forks. To create local copies of the upstream remote, e.g. the master branch, run

.. code-block:: bash

   git checkout master
   git pull upstream master
   git push

The last command makes sure that your forked repo also gets any changes that were pulled to your local clone. Local changes can then conveniently be done by creating branches from the upstream master, via the :bash:`git branch` command.

git branches
============
Modifications are never done directly towards the master or develop branches. Instead, separate branches are created for each new feature. New branches can either be created via the web interface, or with the following command:

.. code-block:: bash

   git checkout -b my-new-feature-branch

Note that this command both creates and switches to the new branch, which is equivalent to running :bash:`git branch my-new-feature-branch` followed by :bash:`git checkout my-new-feature-branch`. Keep in mind that the name of the branch should self-explanatory and reflect the new feature.

Staging and committing changes
------------------------------
Changes are made to a fork in three steps. First, the changes have to be staged locally with the command :bash:`git add`. Second, the changes are added to the local branch via :bash:`git commit`. Third, the changes are added to the fork (or any other remote) with :bash:`git push`. To add all changes, use the :bash:`git add --all` command.

Frequently committing changes is a good practice to keep track of any local changes, and also offers a form of backup when pushed. Keep in mind that each commit should have a clear message that describes what was done, so that changes can be tracked and a clear history exists. A good rule of thumb is that the message should be easy to understand for someone unfamiliar with the details, or by yourself in many years when memory of the change has faded. Before merging, it is common to |squash| several commits into fewer ones, to keep a cleaner log (commit history). In the end, it is all about balance - each relevant feature should be added to a separate commit (unless there is good reason not to), to make it possible to track specific changes.

.. |squash| raw:: html

    <a href="https://docs.gitlab.com/ee/user/project/merge_requests/squash_and_merge.html" target="_blank">squash</a>

To review the commit history, the log feature can be used:

.. code-block:: bash

   git log

Switching between branches
--------------------------
To switch to another local branch, simply run the following command:

.. code-block:: bash

   git checkout some-branch

Remote branches can similarly be checked out by adding the name of the remote before the branch name:

.. code-block:: bash

   git fetch some-remote
   git checkout some-remote/some-branch

Here, the first command, :bash:`git fetch`, synchronizes with the remote, to make sure that information is up to date. To create (and checkout) a local copy of a remote branch, run instead:

.. code-block:: bash

   git checkout -b my-local-branch-name some-remote/some-branch

To see all the local branches, run the following command:

.. code-block:: bash

   git branch

To see all fetched branches, run instead:

.. code-block:: bash

   git branch -v

Note that in newer versions of git, :bash:`git switch` is another command for changing branch. This command is clearer and preferable, since git checkout has multiple other uses.

Furthermore, note that it is sometimes not possible to change branch if there are changes that have not yet been committed, or stashed. A stash is a form of temporary storage that is convenient to use when switching between branches without having to commit, or when moving changes from one branch to another. To stash changes, simply run:

.. code-block:: bash

   git stash

To retrieve the changes from the stash again, run:

.. code-block:: bash

   git stash pop


Source code style guide
=======================
In order to have a manageable collaboration environment and workflow, we enforce consistent source code style, which we expect any contributors to follow. This will greatly improve the review process of any merge request, and increase the chances that any proposed contribution is accepted. Please make sure that the following style guides are applied upon editing any source. They can either be set up to be applied automatically through your editor upon saving, or manually by running the corresponding formatter from terminal (e.g. clang-format).

For Python code, we use :bash:`black`.

For C++/CUDA source code, we enforce consistent LLVM style with :bash:`clang-format` (version 9). Our clang settings are found in the file :bash:`.clang-format` in the git repository.

The SuperConga Wiki provides additional information on the coding guidelines we follow: |wiki guidelines|

.. |wiki guidelines| raw:: html

    <a href="https://gitlab.com/superconga/superconga/-/wikis/Guidelines" target="_blank">https://gitlab.com/superconga/superconga/-/wikis/Guidelines</a>

Unit tests
==========
Any new core functionality should be properly tested for consistency and validity, in the form of unit tests.

Making a merge request
======================
New contributions are proposed by making a merge request (to the develop branch) in the original SuperConga repository. Updates can still be added to the branch in the form of push commits after the merge request is made, but keep in mind to keep a clear commit history so that it is easy to track any changes. Each merge request (and commit) should therefore come with a detailed explanation and motivation of the proposal.

Review process of a merge request
=================================
Upon receiving a merge request, your code will be reviewed by the SuperConga team. During the review process, feedback is provided, and changes may be required. When and if the proposed change is deemed ready, it will be merged into the original SuperConga repository.
