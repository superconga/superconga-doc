=========================================
 Zero-energy Andreev bound states
=========================================
.. role:: bash(code)
   :language: bash

**Goal:** This guide shows how to study zero-energy Andreev Bound states at the edges of unconventional *d*-wave superconductors, and how the states are Doppler-shifted to finite energies by external fields. In particular, *d*-wave superconductors shaped like octagons and discs are simulated, exposed to external fields. While the superconducting condensate has a diamagnetic response, it is shown that the quasiparticle Andreev bound states have a paramagnetic response.

**Files:** The files used in this guide can be found in the folder :bash:`examples/guides/andreev_bound_state_doppler_shift/` in the |SuperConga repository|.

.. |SuperConga repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/tree/master/examples" target="_blank">SuperConga repository</a>

----

Physics background: Andreev bound states
========================================
Conventional superconductors break global :math:`U(1)`-gauge symmetry. Unconventional superconductors are characterized by breaking additional symmetries, usually leading to a plethora of interesting physical properties, such as highly complicated phase diagrams with competing orders, spontaneous symmetry breaking, topological superconductivity, flat bands, and topological states :cite:p:`d-Matsumoto:1995,d-Sigrist:1998,d-Covington:1997,d-Volovik:2003,d-Bernevig:2013,d-Can:2021a,d-Can:2021b`. As an example to illustrate such properties, this guide considers unconventional *d*-wave superconductors :cite:p:`d-vanHarlingen:1995,d-Tsuei:2000`. The anisotropic pairing-interaction in these superconductors can give rise to topologically protected flat bands :cite:p:`d-Ryu:2002,d-Sato:2011`, in the form of zero-energy Andreev-Bound States, e.g. at *[110]*-interfaces :cite:p:`d-Hu:1994,d-Buchholtz:1995,d-Kashiwaya:2000,d-Lofwander:2000`.  Such interfaces lead to a suppression of the superconducting order parameter over a distance of :math:`\sim 5 \xi_0` from the interface. The broken pairs pile up as a huge density of midgap quasiparticle states at the Fermi surface. When applying an external field, these quasiparticle states have a paramagnetic response as opposed to the diamagnetic response of the condensate :cite:p:`d-Xu:1995,d-Higashitani:1997`. The external field Doppler shifts the zero-energy states to finite energies, and the shift grows with the external field. Similarly, a vortex pushed towards a *[110]* edge will Doppler shift the zero-energy states :cite:p:`d-Graser:2004,d-Iniotakis:2008,d-Nagai:2012`, via its surrounding superflow. When lowering the temperature, the system becomes unstable towards spontaneous symmetry-breaking and *"phase crystallization"*, leading to spontaneous currents and fields which Doppler shift the zero-energy states to finite energies. See the `guide on phase crystals`_.

Self-consistent simulations and results
=======================================
The simulations are run from the project root directory via:

.. code-block:: bash
    :caption: Running the Andreev bound state simulation.
    :name: andreev-bound-state-script-run

    ./examples/guides/andreev_bound_state_doppler_shift/andreev_bound_states_flux_sweep.sh

.. container:: toggle

    .. container:: header

        **Click here to show/hide shell script file:** *andreev_bound_states_flux_sweep.sh*

    .. code-block:: bash
        :caption: examples/guides/andreev_bound_state_doppler_shift/andreev_bound_states_flux_sweep.sh
        :name: andreev-bound-state-script
        
        #!/bin/bash

        # Force decimal separator to be point (instead of e.g. comma).
        export LC_NUMERIC="en_US.UTF-8"
        
        # Shell script to simulate an zero-energy Andreev bound states in a d-wave
        # superconducting discs and octagons, for different external fluxes.
        # Note, this script is meant to be executed from the root folder of the project.
        
        # The configuration file to use.
        CONFIG_OCTAGON="examples/guides/andreev_bound_state_doppler_shift/dwave_octagon.json"
        CONFIG_DISC="examples/guides/andreev_bound_state_doppler_shift/dwave_disc.json"
        
        # Base path to save the data to.
        DATA_PATH_OCTAGON="data/andreev_bound_states_flux_sweep/octagon"
        DATA_PATH_DISC="data/andreev_bound_states_flux_sweep/disc"
        
        # Post-processing config to use.
        POST_PROCESS_OCTAGON="examples/guides/andreev_bound_state_doppler_shift/postprocess_abs_flux_sweep.json"
        
        # Loop over fluxes in steps of 0.5 Phi_0/Area.
        B_MIN=0.0
        B_MAX=2.0
        B_STEP=0.5
        for B in $(seq $B_MIN $B_STEP $B_MAX); do
            # Run simulation.
            PATH_OCTAGON=`printf "$DATA_PATH_OCTAGON/T%.6f/B%.6f" $T $B`
            echo "Data path: $PATH_OCTAGON"
            # Run simulation.
            python superconga.py --file $CONFIG_OCTAGON -S $PATH_OCTAGON --no-visualize -B $B
            
            # Post-process: calculate LDOS.
            python postprocess.py --file $POST_PROCESS_OCTAGON --load-path $PATH_OCTAGON
            
            # Run simulation for disc for zero field, and one low field. 
            if (( $(echo "$B < 0.6" |bc -l) ));
            then
                PATH_DISC=`printf "$DATA_PATH_DISC/T%.6f/B%.6f" $T $B`
                echo "$B"
                python superconga.py --file $CONFIG_DISC -S $PATH_DISC --save-data --no-visualize -B $B
            fi
        done

This will run multiple simulations of *d*-wave superconductors, where each simulation has a unique value of the external field (both for the octagon and the disc). **Note:** These simulations might take quite some time due to the high spatial resolution and the postprocessing which calculates the local density of states (LDOS).

The following shows the contents for the octagon configuration file, and the postprocessing config-file used to calculate the density of states. Please note that the data path is overwritten by command-line arguments in the shell script file above.

.. code-block:: bash
    :caption: examples/guides/andreev_bound_state_doppler_shift/dwave_octagon.json
    :name: config-file-andreev-bound-states-octagon

    {
        "physics": {
            "temperature": 0.5,
            "external_flux_quanta": 0.0,
            "penetration_depth": 8.0,
            "crystal_axes_rotation": 0.0,
            "gauge_solenoid": false,
            "order_parameter": {
                "dx2-y2": {
                    "critical_temperature": 1.0,
                    "initial_phase_shift": 0.0,
                    "initial_phase_winding": 0.0,
                    "initial_noise_stddev": 0.0
                }
            }
        },
        "geometry": [
            {
                "regular_polygon": {
                    "add": true,
                    "side_length": 21.0,
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "num_edges": 8,
                    "rotation": 0.0
                }
            }
        ],
        "accuracy": {
            "convergence_criterion": 1e-05,
            "energy_cutoff": 16.0,
            "num_fermi_momenta": 32,
            "num_iterations_burnin": 0,
            "num_iterations_max": 1000,
            "num_iterations_min": 0,
            "points_per_coherence_length": 20.0,
            "vector_potential_error": 1e-06
        },
        "accelerator": {
            "adaptive": true,
            "anderson_always": true,
            "inertia": 0.5,
            "max_mixing": 1.0,
            "min_mixing": 1.0,
            "rank": 1,
            "replace_oldest": true,
            "step_size": 2.0
        },
        "misc": {
            "data_format": "h5",
            "load_path": "",
            "save_frequency": -1,
            "save_path": "data/examples/andreev_bound_states_flux_sweep/octagon",
            "verbose": true,
            "visualize": true
        }
    }
    
.. code-block:: bash
    :caption: examples/guides/andreev_bound_state_doppler_shift/postprocess_abs_flux_sweep.json
    :name: postprocess-config-file-andreev-bound-states

    {
        "convergence_criterion": 1e-4,
        "energy_max": 0.5,
        "energy_min": 0.0,
        "energy_smearing": 0.008,
        "num_energies": 256,
        "num_energies_per_block": 32,
        "num_fermi_momenta": 720,
        "num_iterations_max": 1000,
        "num_iterations_min": 10,
        "load_path": "data/abs_flux_sweep/octagon/B1.000000/T0.500000",
        "save_path": "",
        "verbose": true
    }

After running the simulations and producing the data with the shell-scripts above, the following python script can be run (from the root directory):

.. code-block:: bash
   :caption: Generating the Andreev bound state plot.
   :name: plotting-andreev-bound-state

   python3 examples/guides/andreev_bound_state_doppler_shift/plot_abs_doppler_shift.py

which should generate the following plot:

.. figure:: ../_static/images/guides/abs_doppler_shift.png

    Figure: Unconventional superconducting grains with *d*-wave pairing symmetry, in an external field and at :math:`T=0.5T_{\mathrm{c}}`. The top row shows the order-parameter magnitude for (a) an octagon, and (b) a disc. Superconductivity is suppressed close to *[110]* interfaces due to pair-breaking caused by resonant Andreev reflection, leading to dispersionless quasiparticle states at zero energy. (c)--(d) Applying an external field, these quasiparticle states give rise to a paramagnetic Meissner response, as opposed to the diamagnetic response of the condensate, as seen by the positive and negative parts of the induced magnetic flux density, respectively. Here, the external field is :math:`B_{\mathrm{ext}} = 0.5\Phi_0/\mathcal{A}` with :math:`\kappa = 8`. The external field leads to a superflow response which Doppler shifts the midgap quasiparticle states to finite energies, as illustrated in the DOS in (e), plotted at the *[110]* interface indicated by the cross symbol in panel (c). The strength of the Doppler shift is proportional to the external field strength. Here, a DOS smearing of :math:`\delta = 0.05` was used.

The LDOS can also be inspected interactively by running :bash:`ldos_plotter.py`, e.g. for the data with :math:`\Phi_{\mathrm{ext}}=1.0\Phi_0`:

.. code-block:: bash
   :caption: Interactive spectroscopy of the Andreev bound states.
   :name: interactive-ldos-andreev-bound-state

   python3 ldos_plotter.py --load-path data/andreev_bound_states_flux_sweep/octagon/B1.000000/T0.500000/

which should look something like the following screenshot:

.. figure:: ../_static/images/guides/abs_doppler_shift_interactive.png

    Figure: Screenshot of the interactive spectroscopy tool, :bash:`ldos_plotter.py`, used to study the *d*-wave octagon at :math:`B_{\mathrm{ext}} = 1.0\Phi_0/\mathcal{A}`. The left panel shows the LDOS versus coordinates, at zero energy (as indicated by the interactive slider in the bottom left corner). The right panel shows the DOS averaged over the entire grain (black line), and the LDOS averaged over the red square (red line), corresponding to a STM point-contact of roughly :math:`3\xi_0` as controlled by the slider in the bottom right corner. The LDOS at the corner shows a huge peak of midgap states, that are slightly shifted by the external field.


.. _`guide on phase crystals`: phase-crystals.html


----

References
==========

.. bibliography::
    :labelprefix: D
    :keyprefix: d-
    :style: plain
