===========================
 Download and installation
===========================
.. role:: bash(code)
   :language: bash

Download
========

.. include:: download_license.rst

The code can be downloaded or cloned directly via the above link. The |difference_between_downloading_and_cloning| is that downloading only creates a copy of the source code, while cloning establishes a connection with the remote repository, making it easier to keep the code up-to-date, or even  `contributing`_  and proposing changes to the official source code. To clone and sync with the official repository, make sure that the :bash:`git` package has been installed on your local system, e.g. by running :bash:`sudo apt install git` in Ubuntu/Debian. If using the |SSH| method, make sure to first |generate_SSH_key| and |add_the_ssh_key_to_your_account|.

To learn more about cloning a repository, see the |clone_guide|.

.. |difference_between_downloading_and_cloning| raw:: html

   <a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#download-vs-clone target="_blank">difference between downloading and cloning</a> 

.. _`contributing`: contribute.html

.. |clone_guide| raw:: html

   <a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository" target="_blank">official GitLab cloning guide</a>

.. |SSH| raw:: html

   <a href="https://docs.gitlab.com/ee/ssh/" target="_blank">SSH</a>


.. |generate_SSH_key| raw:: html

   <a href="https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair" target="_blank">generate an SSH key pair</a>

.. |add_the_ssh_key_to_your_account| raw:: html

   <a href="https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account" target="_blank">add the SSH key to your GitLab account</a>



Installation and requirements
=============================
The installation guide and requirements (both hardware and software) can be found in the |README|.

.. |README| raw:: html

   <a href="https://gitlab.com/superconga/superconga#hardware-requirements" target="_blank">README on the GitLab repository</a>
