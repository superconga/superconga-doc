=================================================
 Phase crystals and spontaneous symmetry breaking
=================================================
.. role:: bash(code)
   :language: bash

**Goal:** This guide shows how to study the phenomenon of "phase crystallization", in unconventional *d*-wave superconductors that are shaped like octagons and discs. In particular, it is shown how phase crystallization leads to spontaneous currents and fields at pairbreaking *[110]* interfaces, to Doppler shift zero-energy states to finite energies.

**Files:** The files used in this guide can be found in the folder :bash:`examples/guides/phase_crystal/` in the |SuperConga repository|.

.. |SuperConga repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/tree/master/examples" target="_blank">SuperConga repository</a>

----

Physics background: Phase crystals
==================================
While the edge-states in the `guide on zero-energy Andreev Bound states`_ were enforced by topology, these states are thermodynamically unstable, since they increase the free energy by breaking superconducting pairs. The free-energy contribution of the zero-energy states scale as :math:`T^{-1}`, making them extremely costly at low temperatures. As a consequence, the states make the system unstable against spontaneous symmetry breaking and low-temperature phases, which can lift the degeneracy of the zero-energy states and shift or split them to finite energies. Several such phases have been studied over the recent decades, such as the appearance of a subdominant *s*-wave order :cite:p:`b-Matsumoto:1995,b-Sigrist:1996,b-Fogelstrom:1997,b-Tanuma:1998`, spin-splitting via a spontaneous magnetic ordering :cite:p:`b-Honerkamp:2000,b-Potter:2014`, and spontaneous translationally invariant superflows :cite:p:`b-Higashitani:1997,b-Fauchere:1999,b-Barash:2000,b-Lofwander:2000,b-Suzuki:2014`. The latter scenario has been intensely studied in recent years. In particular, it has been shown that apart from breaking time-reversal symmetry, it is even more energetically favorable to also break continuous translational symmetry along the *[110]* interface :cite:p:`b-Vorontsov:2009,b-Hakansson:2015,b-Holmvall:2018b,b-Holmvall:2019,b-Holmvall:2020,b-Wennerdal:2020,b-Chakraborty:2021`, leading to a unifying description referred to as "*phase crystals*" :cite:p:`b-Holmvall:2020`.

Phase crystals are a class of non-uniform superconducting ground states, driven by the order-parameter phase and its variations rather than by the amplitude, with periodic modulations :math:`\chi(\mathbf{R}) = C_{\mathbf{q}}\cos(\mathbf{q}\cdot\mathbf{R})` at finite wave vector :math:`\mathbf{q}`. Here, :math:`C_{\mathbf{q}}` is a finite Fourier amplitude and the emergent order parameter, which through non-local correlations gives rise to currents and magnetic fields. The general conditions for phase crystallization has been derived for both bulk systems and systems with boundaries, and rely on the properties and symmetries of the superfluid-density tensor :math:`\boldsymbol{\rho}`. Depending on the symmetries of the system, phase crystals might take on different shapes, e.g. checkerboard and stripe patterns :cite:p:`b-Holmvall:2020,b-Holmvall:2019b`. These conditions are naturally satisfied by resonant Andreev-reflection at *[110]* interfaces of unconventional *d*-wave and *p*-wave superconductors, and in conventional S-I-F junctions. In these systems, the phase takes the form :math:`\chi(x) = C(1+y/y_0)e^{-y/y_0}\cos(q_xx)`. Phase crystals extend the paradigm of non-uniform superconducting ground states, as it is an instability of the superconducting state that can occur in the absence of external fields at temperatures :math:`T\leq T^*\approx 0.2T_{\mathrm{c}}`. In contrast, the Abrikosov-vortex state and the Fulde-Ferell-Larkin-Ovchinnikov state (FFLO) :cite:p:`b-Fulde:1964,b-Larkin:1964,b-Buzdin:2007,b-Buzdin:2012,b-Kinnunen:2018` are both instabilities of the normal state, triggered by an external magnetic field at temperatures :math:`T_{\mathrm{c}1}(B) < T < T_{\mathrm{c}2}(B)`. Furthermore, the energetics of these two states are dominated by the order-parameter amplitude and its variations, rather than by the order-parameter phase as for phase crystals.

It is noted that phase crystals do not rely on a confined geometry or mesoscopic effects, but can occur at a semi-infinite superconductor interface. Apart from being studied analytically with quasiclassics and Ginzburg-Landau, and numerically with quasiclassics, it has been studied using tight-binding methods :cite:p:`b-Wennerdal:2020,b-Chakraborty:2021`. The numeric studies show that it is robust against perturbations such as external fields :cite:p:`b-Holmvall:2018b` and geometric distortions :cite:p:`b-Holmvall:2019`, against a subdominant *s*-wave order parameter :cite:p:`b-Hakansson:2015`, and against impurities in strongly correlated systems :cite:p:`b-Chakraborty:2021` (such as the cuprates).


Self-consistent simulations and results
=======================================
The simulations are run from the project root directory via:

.. code-block:: bash
    :caption: Running the phase crystal simulation.
    :name: phase-crystal-script-run

    ./examples/guides/phase_crystal/phase_crystal.sh

The contents of the shell script file is shown below:

.. code-block:: bash
    :caption: examples/guides/phase_crystal/phase_crystal.sh
    :name: phase-crystal-script
    
    #!/bin/bash

    # Shell script to simulate phase crystallization at [110] interfaces of unconventional d-wave superconductors.
    # Note, this script is meant to be executed from the root folder of the project.

    # The configuration file to use.
    CONFIG_OCTAGON="examples/guides/phase_crystal/phase_crystal_octagon.json"
    CONFIG_DISC="examples/guides/phase_crystal/phase_crystal_disc.json"

    # Base path to save the data to.
    DATA_PATH_OCTAGON="data/phase_crystal/octagon/T0.100000"
    DATA_PATH_DISC="data/phase_crystal/disc/T0.100000"

    # Post-processing config to use.
    POST_PROCESS_CONFIG="examples/guides/phase_crystal/postprocess_phase_crystal.json"

    # Run simulations with small external field to seed the phase crystal.
    python superconga.py --file $CONFIG_OCTAGON -S $DATA_PATH_OCTAGON --no-visualize -c 1e-5 -B 0.01 -k -1
    python superconga.py --file $CONFIG_DISC -S $DATA_PATH_DISC --no-visualize -c 1e-5 -B 0.01 -k -1

    # Run simulations without external field.
    python superconga.py --file $CONFIG_OCTAGON -S $DATA_PATH_OCTAGON --no-visualize --load-path $DATA_PATH_OCTAGON
    python superconga.py --file $CONFIG_DISC -S $DATA_PATH_DISC --no-visualize  --load-path $DATA_PATH_DISC

    # Compute LDOS.
    python postprocess.py --file $POST_PROCESS_CONFIG --load-path $DATA_PATH_OCTAGON
    python postprocess.py --file $POST_PROCESS_CONFIG --load-path $DATA_PATH_DISC

This will first run simulations with a small magnetic field to "*seed*" the phase crystal: a completely real order parameter without any superflow is always a valid solution that might be stabilized in self-consistent simulations, but exhibits a higher free energy below :math:`T^*`. The field induces small phase gradients and Meissner screening currents, which trigger the instability. The fields are then turned off, and the phase crystal instability persists. The last part of the script calculates the density of states via the postprocessing routine. Please note that the calculation of the DOS can take quite some time. See the `tutorials`_ for more in-depth information about simulation settings. **Note:** These simulations might take quite some time, since the shell script also runs postprocessing, calculating the local density of states in each geometry.

The following shows the contents for the octagon configuration file:

.. code-block:: bash
   :caption: examples/guides/phase_crystal/phase_crystal_octagon.json
   :name: config-file-phase-crystal-octagon

    {
        "physics": {
            "temperature": 0.1,
            "external_flux_quanta": 0.0,
            "penetration_depth": 100.0,
            "crystal_axes_rotation": 0.0,
            "gauge_solenoid": false,
            "order_parameter": {
                "dx2-y2": {
                    "critical_temperature": 1.0,
                    "initial_phase_shift": 0.0,
                    "initial_phase_winding": 0.0,
                    "initial_noise_stddev": 0.0
                }
            }
        },
        "geometry": [
            {
                "regular_polygon": {
                    "add": true,
                    "side_length": 20.0,
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "num_edges": 8,
                    "rotation": 0.0
                }
            }
        ],
        "accuracy": {
            "convergence_criterion": 1e-05,
            "energy_cutoff": 16.0,
            "num_fermi_momenta": 32,
            "num_iterations_burnin": 0,
            "num_iterations_max": 1000,
            "num_iterations_min": 0,
            "points_per_coherence_length": 20.0,
            "vector_potential_error": 1e-06
        },
        "accelerator": {
            "adaptive": true,
            "anderson_always": true,
            "inertia": 0.5,
            "max_mixing": 1.0,
            "min_mixing": 1.0,
            "rank": 1,
            "replace_oldest": true,
            "step_size": 2.0
        },
        "misc": {
            "data_format": "h5",
            "load_path": "",
            "save_frequency": -1,
            "save_path": "data/examples/phase_crystal/octagon",
            "verbose": true,
            "visualize": true
        }
    }

After running the simulations and producing the data with the shell-scripts above, the following python script can be run (from the root directory):

.. code-block:: bash
   :caption: Generating the phase crystal plot.
   :name: plotting-phase-crystal

   python3 examples/guides/phase_crystal/plot_phase_crystal.py

which should generate the following plot:

.. figure:: /_static/images/guides/phase_crystal.png

    Figure: Phase crystallization in *d*-wave superconductors shaped like octagons and discs (left and right columns, respectively), with :math:`\kappa=100` and at :math:`T=0.1T_{\mathrm{c}} < T^* \approx 0.2T_{\mathrm{c}}`. Here, :math:`T^*` is the transition temperature, where spontaneous (a)--(b) current density and (c)--(d) magnetic flux density, arise as a means to Doppler shift zero-energy states at *[110]* interfaces (hence healing superconductivity and lowering the free energy). Please note that there are no applied external fields. (e) LDOS at points along the *[110]* interface where the spontaneous currents are maximal and minimal (thick and thin solid lines), here referred to as nodes and anti-nodes respectively, as indicated by the symbols in panel (c). The edge-average LDOS (dashed line) and bulk LDOS (dash-dotted line) are also shown for comparison. Here, a DOS smearing of :math:`\delta = 0.05` was used.

The above figure shows spontaneous currents and magnetic fields at *[110]* interfaces due to phase crystallization, in *d*-wave superconductors at temperature :math:`T=0.1T_{\mathrm{c}}`, shaped like an octagon (left column) and a disc (right column). Panel (e) shows the LDOS on the *[110]* surface where the spontaneous current is maximal (nodes) and minimal (anti-nodes), together with the bulk LDOS and the edge-averaged DOS. The latter gives rise to a temperature-independent broadening of midgap states, consistent with experiments.

The local density of states can also be inspected interactively by running :bash:`ldos_plotter.py`, e.g. for the octagon:

.. code-block:: bash
   :caption: Interactive spectroscopy of the phase crystal.
   :name: interactive-ldos-phase-crystal

   python3 ldos_plotter.py --load-path data/phase_crystal/octagon/T0.100000/

which should look something like the following screenshot:

.. figure:: /_static/images/guides/phase_crystal_interactive.png

    Figure: Screenshot of the interactive spectroscopy tool, :bash:`ldos_plotter.py`, used to study phase crystallization in the *d*-wave octagon. The left panel shows the LDOS versus coordinates, at zero energy (as indicated by the interactive slider in the bottom left corner). There is a significant zero-energy states at the nodes, but very few zero-energy states at the anti-nodes. The right panel shows the DOS averaged over the entire grain (black line), and the LDOS averaged over the red square (red line), corresponding to a STM point-contact of roughly :math:`1.25\xi_0` as controlled by the slider in the bottom right corner.


.. _`tutorials`: ../tutorials.html

.. _`guide on zero-energy Andreev Bound states`: abs-doppler-shift.html

----

References
==========

.. bibliography::
    :labelprefix: B
    :keyprefix: b-
    :style: plain