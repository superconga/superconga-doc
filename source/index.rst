.. superconga-doc documentation master file, created by
   sphinx-quickstart on Fri May  8 19:29:52 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SuperConga's documentation!
======================================

SuperConga is an open-source framework for simulating singlet superconductors in equilibrium confined to 2D mesoscopic grains in a perpendicular external magnetic field. It aims at being both fast and easy to use, enabling research without access to a computer cluster. The core is written in C++ and CUDA, exploiting the embarrassingly parallel nature of the quasiclassical theory of superconductivity by utilizing the parallel computational power of modern GPUs. The framework self-consistently computes both the superconducting order-parameter and the induced vector potential, and finds the current density, free energy, induced flux density, local density of states, as well as the magnetic moment. The simulation can be visualized in real-time with OpenGL. A user-friendly Python frontend is provided, enabling simulation parameters to be defined via intuitive configuration files, or via the command-line interface, without requiring a deep understanding of implementation details. For example, complicated geometries can be created with relative ease. The framework ships with simple tools for visualizing the results, including an interactive plotter for spectroscopy. This makes SuperConga an ideal tool both for educational purposes and scientific research, as demonstrated by its use in `student theses and scientific papers`_.

This documentation contains an extensive user manual on how to set up simulations of various systems and physical scenarios. It also contains technical references on various aspects. The documentation is generated from the |documentation repository|.

.. _`student theses and scientific papers`: about.html#research-using-superconga

.. |documentation repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga-doc" target="_blank">SuperConga-doc repository</a>

Download the code
-----------------

.. include:: download_license.rst

Getting started
---------------
Requirements and installation instructions can be found in the |installation guide|. The `tutorials`_ explain how to use the different features of the framework, while the `technical reference`_ gives a detailed specification of things like parameters and configuration files. The `examples`_ is a showcase to give an idea of different scenarios that can be studied with the framework, while the `physics guides`_ show in detail how to use the framework to study particular superconducting phenomena to do research.

.. |installation guide| raw:: html

   <a href="https://gitlab.com/superconga/superconga#hardware-requirements" target="_blank">installation guide</a>

.. _`tutorials`: tutorials.html
.. _`technical reference`: technical_reference.html
.. _`examples`: examples.html
.. _`physics guides`: guides/guides.html

Citation
--------
Please help us make SuperConga better by `citing it`_ in your publications. In turn, please let us know if you have used SuperConga in your research, so that we can cite and help increase the exposure of your research. `See citation`_ for more information.

.. _`citing it`: cite.html
.. _`See citation`: cite.html


Examples showcase
-----------------

.. figure:: _static/images/showcase.png

      Figure: SuperConga showcase.
      

Funding and support
-------------------

.. include:: funding_support.rst



.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about.rst

   download.rst

   cite.rst

   examples.rst

   tutorials.rst

   guides/guides.rst

   technical_reference.rst

   faq.rst

   contact.rst

   contributors.rst

   contribute.rst

   API documentation <https://superconga.gitlab.io/superconga/>

   Changelog <https://gitlab.com/superconga/superconga/-/blob/master/CHANGELOG.md>