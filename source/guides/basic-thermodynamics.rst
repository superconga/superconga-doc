=====================
 Basic thermodynamics
=====================
.. role:: bash(code)
   :language: bash

**Goal:** This guide shows how to reproduce the basic thermodynamic phase diagram for superconductivity, in a mesoscopic grain. In particular, the scripts in the guide are used to compute the grain-average order parameter, free energy, entropy, and heat capacity, in the temperature range :math:`T/T_{\mathrm{c}} \in (0, 1)`. Both *s*-wave and *d*-wave superconductivity is considered, in a square-shaped geometry of side length :math:`10\xi_0`, where :math:`\xi_0 = \hbar v_{\mathrm{F}}/ 2\pi k_{\mathrm{B}}T_{\mathrm{c}}`is the superconducting coherence length, with Fermi velocity :math:`v_{\mathrm{F}}` and superconducting transition temperature :math:`T_{\mathrm{c}}`.

**Files:** The files used in this guide can be found in the folder :bash:`examples/parameter_sweeps/` in the |SuperConga repository|.

.. |SuperConga repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/tree/master/examples" target="_blank">SuperConga repository</a>

**Extensions:** This example can easily be extended to obtain less trivial phase diagrams.

----

Physics background: superconductivity
=====================================
Superconductivity is characterized by a gap in the excitation spectrum, :math:`2|\Delta|`, and generally arises through a second-order phase transition, associated by a lowering of both the free energy (:math:`\Omega`) and the entropy (:math:`S`), with a jump in the heat capacity (:math:`C`). This lowering of the energy is due to condensation energy of conduction electrons forming Cooper pairs. All pairs are described by the same ground-state wave function, with an amplitude proportional to the superconducting gap, and a phase (:math:`\chi`) which is directly coupled to the pair superfluid momentum (:math:`\mathbf{p}_{\mathrm{s}}`) and the electromagnetic gauge field (:math:`\mathbf{A}`). The pairs are phase coherent over a distance referred to as the superconducting coherence length (:math:`\xi_0`), consequently breaking global :math:`U(1)`-gauge symmetry. The lower symmetry and entropy characterizes a more ordered state, which is quantified by the complex-valued order parameter,

.. math::
    :label: order-parameter

    \Delta(\mathbf{p}_{\mathrm{F}},\mathbf{R}) = |\Delta(\mathbf{R})|e^{i\chi(\mathbf{R})}\eta(\mathbf{p}_{\mathrm{F}}),

which is zero above the superconducting transition (:math:`T_{\mathrm{c}}`) and non-zero below it. Here, :math:`\mathbf{R}` is the center-of-mass coordinate for the Cooper pair, :math:`\mathbf{p}_{\mathrm{F}}` the Fermi momentum, and :math:`\eta` the superconducting basis function. The latter effectively encodes the pairing symmetry of the order parameter.


Self-consistent simulations and results
=======================================
We set out to study an *s*-wave disc with radius :math:`\mathcal{R} = 15\xi_0`, exposed to an external magnetic flux of :math:`\Phi_{\mathrm{ext}} = 1.5\Phi_0`, where :math:`\Phi_0 \equiv hc/2|e|` is the magnetic-flux quantum. Furthermore, we want to vary the penetration depth :math:`\lambda`, ranging from :math:`\kappa \equiv \lambda/\xi_0 = 2` to :math:`\kappa = \infty`. This can be achieved by running the following script from the project root directory:

.. code-block:: bash
    :caption: Running the thermodynamics simulation.
    :name: thermodynamics-script-run

    ./examples/parameter_sweeps/swave_and_dwave_temperature_sweep.sh

This will launch a number of simulations, each with a unique value of the penetration depth. The contents of the shell script file is:

.. code-block:: bash
    :caption: swave_and_dwave_temperature_sweep.sh
    :name: thermodynamics-script
    
    #!/bin/bash

    # Note, this script is meant to be executed from the root folder of the project.
    # ./examples/parameter_sweeps/swave_and_dwave_temperature_sweep.sh

    # Force decimal separator to be point (instead of e.g. comma).
    export LC_NUMERIC="en_US.UTF-8"

    # Note, this script is meant to be executed from the root folder of the project.

    # The configuration file to use.
    CONFIG="examples/parameter_sweeps/temperature_sweep_config.json"

    # Base path to save the data to.
    DATA_BASE_PATH="data/examples/temperature_sweep"

    # The number of burn-in iterations. Negative: run until boundary is converged.
    BURNIN=-1

    # String containing load path (including flag). Empty for first run.
    LOAD_STRING_SWAVE=""
    LOAD_STRING_DWAVE=""

    # Loop over temperatures. Manually add T=0.999 at end.
    T_MIN=0.01
    T_MAX=0.99
    T_STEP=0.02
    T_IDX=0
    for T in $(seq $T_MIN $T_STEP $T_MAX; seq 0.999 $T_STEP 0.999); do
        DATA_PATH_SWAVE=`printf "$DATA_BASE_PATH/swave/T%2.3f/" $T`
        DATA_PATH_DWAVE=`printf "$DATA_BASE_PATH/dwave/T%2.3f/" $T`

        # Run simulation: s-wave.
        python superconga.py simulate -C $CONFIG -S $DATA_PATH_SWAVE --no-visualize -T $T $LOAD_STRING_SWAVE
        # Run simulation: d-wave.
        python superconga.py simulate -C $CONFIG -S $DATA_PATH_DWAVE --no-visualize -T $T $LOAD_STRING_DWAVE --no-s-wave --dx2-y2-wave 1 0 0
        
        # Use data as start-point for next simulation, with burnin.
        LOAD_STRING_SWAVE="-L $DATA_PATH_SWAVE -b $BURNIN"
        LOAD_STRING_DWAVE="-L $DATA_PATH_DWAVE -b $BURNIN"
    done

Here, a for loop is run over a sequence of temperatures. For each temperature, a unique data path is constructed for the *s*-wave and *d*-wave simulations, and they are run with :bash:`python superconga.py simulate`. argument :bash:`-C|--config` specifies the path to the configuration file (the contents of which are shown below), the data output folder is specified by :bash:`-S`, the temperature is set with the argument :bash:`-T` (overriding the temperature in the configuration file), and :bash:`--no-visualize` specifies that no live visualization shall be done during the simulations. See the `tutorials`_ for more in-depth information about simulation settings. Furthermore, each simulation uses the data from the previous one as a start guess, and a burn-in of -1. The latter signifies that the coherence functions at the boundary should be updated to self-consistency when reading data, before starting the main simulation. The very first simulation uses a bulk start guess instead. Since the configuration file specifies an *s*-wave order parameter, the simulation for the *d*-wave has to explicitly remove the former and add the latter, via :bash:`--no-s-wave --dx2-y2-wave 1 0 0`.

The contents of the configuration file is:

.. code-block:: bash
   :caption: examples/parameter_sweeps/temperature_sweep_config.json
   :name: config-file-temperature_sweep

    {
        "physics": {
            "temperature": 0.5,
            "external_flux_quanta": 0.0,
            "penetration_depth": -1.0,
            "crystal_axes_rotation": 0.0,
            "gauge": "symmetric",
            "charge_sign": -1,
            "order_parameter": {
                "s": {
                    "critical_temperature": 1.0,
                    "initial_phase_shift": 0.0,
                    "initial_noise_stddev": 0.0,
                    "vortices": []
                }
            }
        },
        "geometry": [
            {
                "regular_polygon": {
                    "add": true,
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "num_edges": 4,
                    "rotation": 0.0,
                    "side_length": 10.0
                }
            }
        ],
        "numerics": {
            "convergence_criterion": 3e-07,
            "energy_cutoff": 16.0,
            "norm": "l2",
            "num_fermi_momenta": 32,
            "num_iterations_burnin": 0,
            "num_iterations_max": 10000,
            "num_iterations_min": 0,
            "points_per_coherence_length": 10.0,
            "vector_potential_error": 1e-06
        },
        "accelerator": {
            "name": "barzilai-borwein"
        },
        "misc": {
            "data_format": "h5",
            "load_path": "",
            "save_frequency": -1,
            "save_path": "data/examples/temperature_sweep/swave",
            "verbose": true,
            "visualize": true
        }
    }

Note that special care is taken such that all of the edges of the *d*-wave square are :math:`[100]`, hence no pair-breaking :math:`[110]` interfaces (see e.g. `the guide on zero-energy Andreev bound states`_). After running the shell-scripts above, the results can be plotted by running the following Python script from the root directory:

.. code-block:: bash
   :caption: Generating the superconducting phase-diagram plot.
   :name: plotting-thermodynamics

   python examples/parameter_sweeps/plot_temperature_sweep.py

which should generate the following figure:

.. figure:: /_static/images/guides/thermodynamics.png

    Figure: The phase diagram for *s*-wave superconductivity (solid lines) and *d*-wave superconductivity (dashed lines) in a mesoscopic grain. (a) The grain-average order parameter, with area :math:`\mathcal{A} = 10\xi_0 \times 10\xi_0`. The other panels show, with respect to the normal state, the (b) free energy, (c) entropy, and (d) heat capacity. Lines are from self-consistent numerics using SuperConga, while symbols are from analytics. Here, the observables are in units of the quantities :math:`\Omega_{0} \equiv \mathcal{A}N_{\mathrm{F}}(k_{\mathrm{B}}T_{\mathrm{c}})^2`, :math:`S_{0} \equiv \mathcal{A}N_{\mathrm{F}}k_{\mathrm{B}}^2T_{\mathrm{c}}`, and :math:`C_{0} \equiv \mathcal{A}N_{\mathrm{F}}k_{\mathrm{B}}^2T_{\mathrm{c}}`, where :math:`N_{\mathrm{F}}` is the normal-state density of states at the Fermi surface.

The figure shows the the temperature dependence of the area-averaged (a) order-parameter amplitude, (b) free energy, (c) entropy, and (d) heat capacity. These are obtained numerically with the SuperConga framework (lines) via simulations of square-shaped mesoscopic grains, with perfectly specular interfaces with side lengths :math:`10 \xi_0`. Apart from the finite geometry, there is no spatial dependence or inhomogeneities present in the simulations, and the grains are essentially equivalent to bulk superconductors. The numeric results therefore reproduce the well-known thermodynamic curves of bulk superconductivity (see e.g. Tinkham :cite:p:`e-Tinkham:2004`), and are thus in excellent agreement with analytic bulk calculations at both the transition temperature and at zero temperature (symbols). A lowering of the free energy and entropy below zero indicates a more ordered ground state. The continuity in entropy and discontinuity in heat capacity at the transition temperature clearly indicates a second-order phase transition.

.. _`tutorials`: ../tutorials.html

.. _`the guide on zero-energy Andreev bound states`: abs-doppler-shift.html

----

References
==========

.. bibliography::
    :labelprefix: E
    :keyprefix: e-
    :style: plain
