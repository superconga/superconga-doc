================
 About
================
.. role:: bash(code)
   :language: bash

What is SuperConga?
===================
SuperConga is a framework for simulating spin-singlet superconductivity in 2D-geometries in equilibrium. It comes with a number of convenient tools, and a user-friendly frontend, which make it easy for users to setup new simulations and analyze the results, without having to write any code.

The underlying machinery is based on the quasiclassical theory of superconductivity, which is a Green-function approach to BCS-Eliashberg-Gorkov theory, including additional states and phenomena. Quasiclassics is a separation of scales, where higher-order terms are either treated as phenomenological parameters taken from experiment, or derived from microscopic theory. For example, boundary conditions are derived from full scattering-matrix theory, making it possible to include the full machinery of non-equilibrium, transport, and spin-dependent effects. Currently, SuperConga is limited to specular boundary conditions, in equilibrium and with spin-degeneracy. Heterogeneous systems have been studied before though, and efforts have been made to include full spin-dependence.

Motivation / Statement of need
------------------------------
The quasiclassical theory of superconductivity has been used to study superconducting phenomena and devices for nearly half a century. Compared to tight-binding methods, it enables self-consistent solutions of relatively large systems. Quasiclassical theory is therefore particularly useful to study electrodynamics of type-II superconductors, mesoscopic physics, heterogeneous systems, and quantum devices. Furthermore, using modern hardware and high-performance computing, relatively complicated simulations might converge in a matter of seconds, opening up the possibility for fast and interactive simulations with live visualization.

However, quasiclassics is non-trivial to implement in both an optimal and a robust way. The lack of open-source software and libraries therefore lead to researchers and students having to spend valuable research time to re-implement the same algorithms, often falling in the same pitfalls. Furthermore, there are very few papers dedicated to the details of numeric implementation, making some of the numeric challenges un-resolved. The motivation for developing and releasing SuperConga and its documentation is to address these issues, by implementing and sharing best practices, and allowing the community to contribute. 


Goals
-----
The main goals of SuperConga is to provide a framework that is:

- **Easy to use:** running simulations should not require writing source-code, or a profound knowledge of the underlying machinery.

  - Solution: a user-friendly frontend to set up simulations, and convenient tools to do post-processing and data analysis.
- **Flexible and modular:** it should be easy to add and modify functionality. It should also be possible to use the various components of the framework independently for specific tasks, or together to set up advanced simulations.

  - Solution: writing the framework as an object-oriented API.
- **Fast:** users without the access to state-of-the-art computer clusters should still be able to do research, while users with such access should be able to push the boundaries of simulations of superconductivity.

  - Solution: exploiting the parallel nature of quasiclassics, taking full advantage of high-performance computing on Graphics Processing Units (GPUs). By parallelizing in several tiers, the code should run efficiently even on a consumer laptop or desktop.
- **Robust:** finding the ground state in superconducting systems requires self-consistent solutions in a high-dimensional phase space, where there are many minima and meta-stable states.

  - Solution: we have developed a sophisticated and adaptive optimizer, called CongAcc, which is both fast and robust. We have also implemented extensive unit testing, and try to let best practices guide our software development.
- **Open source:** by coming together as a community, we can share best practices to find the best numeric implementations of quasiclassical theory, pushing the frontiers of condensed matter research.


Key features
------------
- **User-friendly frontend**, making it easy to set up advanced simulations.
- **Powerful backend**, harnessing the power of massive parallelization on GPUs.
- **Live visualization** of simulation progress using OpenGL.
- **General 2D geometries**, set up using a combination of discs and polygons.
- **Self-consistency solver** for both the superconducting gap and vector potential.
- **Riccati ODE solver** for the quasiclassical Eilenberger equation in non-uniform environments.
- **Robust and advanced optimizer**, with several different strategies to solve self-consistency, including an adaptive method.
- **Off-loading** to robust external libraries, like |Thrust|, |Armadillo|, BLAS and LAPACK.
- **Post-processing** of data, e.g. calculating the density of states from a converged solution.
- **Data analysis/visualization** of computed quantities, including an interactive spectroscopy tool.

----

.. |Thrust| raw:: html

    <a href="https://developer.nvidia.com/thrust" target="_blank">Thrust</a>

.. |Armadillo| raw:: html

    <a href="http://arma.sourceforge.net/" target="_blank">Armadillo</a>


Research using SuperConga
=========================
Below follows a list of publications using SuperConga, highlighting its applicability to interesting physical scenarios and scientific research. These publications are also a great source for in-depth details on the physics and theory underlying SuperConga. Please contact us if you have any research that you would like to be added to the list of examples using SuperConga.

Papers
------
* M\. Håkansson, T. Löfwander, and M. Fogelström, "|nat_phys_2015|", Nature Physics **11**, 755-760 (2015).
* P\. Holmvall, A. B. Vorontsov, M. Fogelström, and T. Löfwander, "|nat_com_2018|", Nature Communications **9**, 2190 (2018).
* P\. Holmvall, T. Löfwander, and M. Fogelström, "|j_conf_2018|" J. Phys. Conf. Ser. **969**, 012037 (2018).
* P\. Holmvall, A. B. Vorontsov, M. Fogelström, and T. Löfwander, "|prb_2019|" Physical Review B **99**, 184511 (2019).
* P\. Holmvall, M. Fogelström, T. Löfwander, and A. B. Vorontsov, "|phase_crystals_2020|", Physical Review Research **2**, 013104 (2020).
* N\. Wall Wennerdal, A. Ask, P. Holmvall, T. Löfwander, and M. Fogelström, "|bdg_comparison_2020|", Physical Review Research **2**, 043198 (2020).
* P\. Holmvall, N. Wall-Wennerdal, M. Håkansson, P. Stadler, O. Shevtsov, T. Löfwander, and M. Fogelström, "|superconga_2023|", Applied Physics Reviews 10, 011317 (2023).
* P\. Holmvall, A. M. Black-Schaffer, "|coreless_vortex_2023|", Physical Review B 108, L100506 (2023).
* P\. Holmvall, N. Wall-Wennerdal, A. M. Black-Schaffer, "|coreless_vortex_robust_2023|", Physical Review B 108, 094511 (2023).
* P\. Holmvall, A. M. Black-Schaffer, "|mesoscopic_current_enhancement_2023|", Physical Review B 108, 174505 (2023).
* K\. M. Seja, N. Wall-Wennerdal, T. Löfwander, A. M. Black-Schaffer, M. Fogelström, P. Holmvall "|impurity_temperature_phase_crystal_2024|", arXiv:2412.14876 [cond-mat.supr-con] (2024).
* P\. Holmvall, A. M. Black-Schaffer, "|designing_edge_currents_2025|", arXiv:2501.14563 [cond-mat.supr-con] (2025).

.. |nat_phys_2015| raw:: html

    <a href="https://www.nature.com/articles/nphys3383" target="_blank">Spontaneously broken time-reversal symmetry in high-temperature superconductors</a>

.. |nat_com_2018| raw:: html

    <a href="https://www.nature.com/articles/s41467-018-04531-y" target="_blank">Broken translational symmetry at edges of high-temperature superconductors</a>

.. |j_conf_2018| raw:: html

    <a href="https://iopscience.iop.org/article/10.1088/1742-6596/969/1/012037" target="_blank">Spontaneous generation of fractional vortex-antivortex pairs at single edges of high-Tc superconductors</a>

.. |prb_2019| raw:: html

    <a href="https://journals.aps.org/prb/abstract/10.1103/PhysRevB.99.184511" target="_blank">Spontaneous symmetry breaking at surfaces of d-wave superconductors: Influence of geometry and surface ruggedness</a>

.. |phase_crystals_2020| raw:: html

    <a href="https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.013104" target="_blank">Phase crystals</a>

.. |bdg_comparison_2020| raw:: html

    <a href="https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.043198" target="_blank">Breaking time-reversal and translational symmetry at edges of d-wave superconductors: microscopic theory and comparison with quasiclassical theory</a>

.. |superconga_2023| raw:: html

    <a href="https://doi.org/10.1063/5.0100324" target="_blank">SuperConga: An open-source framework for mesoscopic superconductivity</a>

.. |coreless_vortex_2023| raw:: html

    <a href="https://journals.aps.org/prb/abstract/10.1103/PhysRevB.108.L100506" target="_blank">Coreless vortices as direct signature of chiral d-wave superconductivity</a>

.. |coreless_vortex_robust_2023| raw:: html

    <a href="https://journals.aps.org/prb/abstract/10.1103/PhysRevB.108.094511" target="_blank">Robust and tunable coreless vortices and fractional vortices in chiral d-wave superconductors</a>

.. |mesoscopic_current_enhancement_2023| raw:: html

    <a href="https://journals.aps.org/prb/abstract/10.1103/PhysRevB.108.174505" target="_blank">Enhanced chiral edge currents and orbital magnetic moment in chiral d-wave superconductors from mesoscopic finite-size effects</a>

.. |impurity_temperature_phase_crystal_2024| raw:: html

    <a href="https://arxiv.org/abs/2412.14876" target="_blank">Impurity-temperature phase diagram with phase crystals and competing time-reversal symmetry breaking states in nodal d-wave superconductors</a>

.. |designing_edge_currents_2025| raw:: html

    <a href="https://arxiv.org/abs/2501.14563" target="_blank">Designing edge currents using mesoscopic patterning in chiral d-wave superconductors</a>



Theses
------
* N\. Wennerdal, "|wennerdal_msc|", MSc thesis, (2011).
* M\. Håkansson, "|hakansson_lic|", Licentiate thesis, (2015).
* P\. Holmvall, "|holmvall_lic|", Licentiate thesis, (2017).
* P\. Holmvall, "|holmvall_phd|", PhD thesis, (2019).

.. |wennerdal_msc| raw:: html

    <a href="https://hdl.handle.net/20.500.12380/146286" target="_blank">Parallel Computations of Vortex Core Structures in Superconductors</a>

.. |hakansson_lic| raw:: html

    <a href="https://research.chalmers.se/en/publication/211094" target="_blank">Mesoscopic thin film superconductors - A computational framework</a>

.. |holmvall_lic| raw:: html

    <a href="https://research.chalmers.se/en/publication/253315" target="_blank">Modeling mesoscopic unconventional superconductors</a>

.. |holmvall_phd| raw:: html

    <a href="https://research.chalmers.se/en/publication/513562" target="_blank">Crystallization of the superconducting phase in unconventional superconductors</a>

----

The SuperConga framework
========================
Below follows a short summary of technical details on the SuperConga framework.

Framework overview
------------------
A typical simulation with SuperConga can be divided into the following steps:

1. **pre-processing:** setting up simulation parameters,
2. **processing:** running the self-consistent simulation,
3. **post-processing:** data analyzing and calculating additional quantities.

A main philosophy is that SuperConga should be possible to use without ever having to write/edit source code. To aid in this, SuperConga comes with a user-friendly frontend, :bash:`superconga.py`, which makes it easier to set up a simulation and define all simulation parameters. This is done via a configuration file (in |.json-format|), and supplemented by command-line arguments. The frontend parses and validates the parameters, and if valid, sends them to the backend (binaries), which perform the actual computations and simulation, by calling on the various functionalities in the framework. The results are then output to terminal and/or saved to disk, whichever the user prefers. Please note that it is still perfectly viable to directly use the full SuperConga framework by writing your own C++/CUDA source code. This allows for setting up more tailored and advanced simulations. The goal, however, is to make the frontend sufficiently versatile and powerful, such that standard users never have to do this.

.. |.json-format| raw:: html

    <a href="technical_reference.html" target="_blank">.json-format</a>

Below is a simplified sketch of the UI/workflow.

.. figure:: _static/images/code/user_flow_chart.png

        Sketch of SuperConga workflow.

Quick usage example
-------------------
After successfully |building and compiling| the framework, running a simulation (in this case of a mesoscopic Abrikosov-vortex lattice) is as simple as calling the following from the repository root directory:

.. |building and compiling| raw:: html

    <a href="https://gitlab.com/superconga/superconga#setup-and-compilation" target="_blank">building and compiling</a>

.. code-block:: bash
   :caption: **Quick usage example: s-wave vortex lattice**
   :name: about-quick-usage

   python superconga.py simulate --config examples/swave_abrikosov_lattice/

Assuming that the data was saved to :bash:`data/swave_abrikosov_vortex_lattice/`, the following command will plot the data, which will yield the plot shown below:

.. code-block:: bash
   :caption: **Plotting the data from the s-wave vortex lattice**
   :name: about-quick-usage-plotting

   python superconga.py plot-simulation --load-path data/examples/swave_abrikosov_lattice/

.. figure:: _static/images/simulation_example.png

        Plot from example simulation: *s*-wave Abrikosov-vortex lattice.


To learn more about running simulations, see the `tutorials`_ and `physics guides`_.

.. _`tutorials`: tutorials.html
.. _`physics guides`: guides/guides.html

..
    We should add something short about implementation performance here, when available.

License
=======

.. include:: download_license.rst
