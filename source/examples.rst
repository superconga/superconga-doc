==================
 Examples showcase
==================
.. role:: bash(code)
   :language: bash

This page contains an example showcase, illustrating different systems and physical scenarios studied with SuperConga. They are based on the existing examples in the SuperConga repository, in the folder :bash:`examples/`. There, a README can be found describing each example in detail.

To run the examples and reproduce the plots, first run SuperConga from the root folder with :bash:`python superconga.py simulate --config examples/<example>/simulation_config.json`, where :bash:`<example>` is the name of the example. Second, upon successfully completing the simulation, the data can be visualized via :bash:`python superconga.py plot-simulation --load-path data/examples/<example>/`. This should generate the same plots seen below.

Additionally, once the simulations have been run, the LDOS can be calculated and plotted in a similar way, using :bash:`python superconga.py postprocess` and :bash:`python superconga.py plot-postprocess`, respectively. See the `tutorials`_ and command-line help messages (:bash:`python superconga.py --help`) for more information.

To see a larger version of each image, open them in a new tab.

.. _`tutorials`: tutorials.html

----

Meissner-state
==============
The following is a demonstration of the Meissner state in a conventional *s*-wave superconductor, shaped like a disc.
   
.. figure:: _static/images/examples/swave_disc_meissner.png

----

A conventional Abrikosov vortex
===============================
This example shows a simulations of a single Abrikosov vortex in a disc-shaped *s*-wave superconductor.

.. figure:: _static/images/examples/swave_disc_vortex.png

----

A mesoscopic Abrikosov-vortex lattice
=====================================
This example shows a simulations of a mesoscopic Abrikosov-vortex lattices in a conventional *s*-wave superconductor, shaped like a square.

.. figure:: _static/images/examples/swave_abrikosov_lattice.png

----

Zero-energy states
==================
This example shows a simulations of an unconventional *d*-wave superconductor, shaped like an octagon with four pairbreaking :bash:`[110]`-interfaces and four non-pairbreaking :bash:`[100]`-interfaces.

.. figure:: _static/images/examples/dwave_octagon.png

----

Phase crystallization
=====================
This example shows how zero-energy Andreev Bound States are Doppler shifted to finite energies by spontaneous superflow, thus lowering the free energy in the form of regained condensation energy. The (non-local) response to the superflow are finite equilibrium currents circulating in loops, giving rise to spontaneous magnetic fields.

.. figure:: _static/images/examples/dwave_phase_crystal.png

----

Multi-component superconductor
==============================
This example shows a superconductor with a subdominant order: :math:`d+is`.

.. figure:: _static/images/examples/dwave_plus_swave.png

----

Chiral d-wave superconductor
============================
This example shows a chiral *d*-wave superconductor.

.. figure:: _static/images/examples/dwave_chiral.png
