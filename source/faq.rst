=========================
 FAQ and troubleshooting
=========================
.. role:: bash(code)
   :language: bash

This page contains Frequently Asked Questions (FAQ), troubleshooting questions and solutions, as well as listing some know issues with the current version of the code.

----

Frequently Asked Questions
==========================
.. role:: bash(code)
   :language: bash

Q: I think I have found a bug or an issue. How do I report it?
**************************************************************
Open a |bug issue| in the GitLab repository. Please make sure to first browse and search through the issues for a similar one, before opening a new one.

.. |bug issue| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/issues/new?issuable_template=Bug" target="_blank">Bug issue</a>


Q: I have a suggestion for how to make the code better. How can I contribute?
*****************************************************************************
We have a long backlog of features we want to add (and issues to solve), but we welcome new ideas and suggestions. If you have suggestions then either `contact`_ us, or open a new |issue| in the repository with the "Discussion" label.  See the `contribution guide`_ for more information on how to contribute to the project.

.. _`contact`: contact.html

.. _`contribution guide`: contribute.html

.. |issue| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/issues/new" target="_blank">issue</a>


Q: How do I cite SuperConga in my project?
******************************************
Please see the `citation page`_.

.. _`citation page`: cite.html


Q: Is it possible to run SuperConga on AMD hardware?
****************************************************
There is experimental support for running on AMD GPUs using ROCm/HIP, |amd_gpu|.


Q: Is it possible to run SuperConga on CPU only (without GPU)?
**************************************************************
Not presently, but this is definitely a feature we would like to implement.

.. |amd_gpu| raw:: html

   <a href="https://gitlab.com/superconga/superconga#hardware-requirements" target="_blank">see main README</a>

----

Troubleshooting
===============

Q: I'm getting errors during CUDA part of 'python superconga.py setup'
**********************************************************************
Try manually specifying the CUDA architecture by adding :bash:`--cuda-arch XY`, where `XY` is a two-digit number (for example `30`, `61`, `75`, etc.). The number describes what hardware you have. E.g. NVIDIA GTX 1080 Ti = `61`, and NVIDIA RTX 2080 Ti = `75`. The setup will typically print the CUDA architecture found on your system, i.e. look for something like: :bash:`Autodetected CUDA architecture(s): 5.3;6.0;6.1;7.0;7.5;8.0;8.6;8.6+PTX`, where :bash:`8.6` would be added via :bash:`--cuda-arch 86`.

Q: The residual, free energy, or another quantity is "nan"
**********************************************************
This might occur when running with single floating-point precision (32bit). Try changing to double floating-point precision (64bit) with the command-line argument :bash:`-P 64`.

Q: I'm getting an error similar to "KeyError: 'physics'"
********************************************************
This might be caused by not specifying a valid configuration file, i.e. it is missing or has improperly formatted :bash:`json` entries. Please refer to the section on configuration files in the |technical reference|.

.. |technical reference| raw:: html

    <a href="technical_reference.html" target="_blank">technical reference</a>

Q: Why do I get an empty output when running with a high resolution?
********************************************************************
This is most likely caused by your GPU running out memory. Try reducing the number of energies per block (i.e. less parallelism).

Q: Why do I get strange spikes in the DOS, e.g. a split zero-energy peak in Vortices?
*************************************************************************************
This occurs when running with too small spatial resolution. As a rule of thumb, one typically needs to run with 20 points per coherence lengths to accurately compute. This might lead to running out of memory for large enough systems (see the question about running out of memory).

Q: Why am I getting an error from json.decoder.JSONDecodeError?
***************************************************************
If you're getting an error similar to any of the following lines, it might be an indication that the syntax in your :bash:`.json` file is wrong:

- :bash:`json.decoder.JSONDecodeError: Expecting ',' delimiter: line XX column X (char XXX)`
- :bash:`json.decoder.JSONDecodeError: Expecting value: line XX column X (char XXX)`
- :bash:`json.decoder.JSONDecodeError: Expecting property name enclosed in double quotes: line X column X (char XXX)`

JSON can be very picky with with syntax. Please make sure that all brackets are closed, that no additional comma delimiters are added, and that no comma delimiter are missing. Please see the example :bash:`config.json` files in the :bash:`examples/` folders.

----

Known issues
============

X forwarding
************
There is a known issue with X forwarding not working properly. A temporary workaround is to disable X forwarding completely, and running with :bash:`--no-visualize`.

Boundary artefact
*****************
There is a known issue with certain boundaries, caused by boundary interpolation. This occurs mainly for trajectories that are nearly parallel with "internal" boundaries, for example when creating holes in the superconductors.
