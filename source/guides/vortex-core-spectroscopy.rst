=========================================
 Vortex-core spectroscopy
=========================================
.. role:: bash(code)
   :language: bash

**Goal:** This guide shows how to study the local density of states (LDOS) in vortex cores of *s*-wave and *d*-wave superconductors. In particular, the LDOS as a function of distance from the vortex core is studied, and it is shown that the *s*-wave core has a continuous rotational symmetry while the *d*-wave core has a 4-fold rotational symmetry (mimicking the *d*-wave basis function). It is also shown how to use the interactive LDOS plotter. To see how to study the spatial dependence of the currents and fields, see instead the `guide on Abrikosov-vortex cores`_.

.. _`guide on Abrikosov-vortex cores`: abrikosov-vortex-core.html

**Files:** The files used in this guide can be found in the folder :bash:`examples/guides/vortex_core_spectroscopy/` in the |SuperConga repository|.

.. |SuperConga repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/tree/master/examples" target="_blank">SuperConga repository</a>

----

Physics background: Vortex core spectroscopy
============================================
...

Self-consistent simulations and results
=======================================
The simulations are run from the project root directory via:

.. code-block:: bash
    :caption: Running the vortex-core spectroscopy simulation.
    :name: vortex-core-spectroscopy-script-run

    ./examples/guides/vortex_core_spectroscopy/vortex_core_spectroscopy.sh

The following shows the contents of the shell-script file:

.. code-block:: bash
    :caption: examples/guides/vortex_core_spectroscopy/vortex_core_spectroscopy.sh
    :name: vortex-core-spectroscopy-script
    
    #!/bin/bash

    # Shell script to calculate the spectra of vortex cores in s-wave and d-wave superconductors.
    # Note, this script is meant to be executed from the root folder of the project.

    # The configuration file to use.
    CONFIG_SWAVE="examples/guides/vortex_core_spectroscopy/swave_vortex.json"
    CONFIG_DWAVE="examples/guides/vortex_core_spectroscopy/dwave_vortex.json"

    # Base path to save the data to.
    DATA_PATH_SWAVE="data/vortex_core_spectroscopy/swave/T0.500000"
    DATA_PATH_DWAVE="data/vortex_core_spectroscopy/dwave/T0.500000"

    # Post-processing config to use.
    POST_PROCESS_CONFIG="examples/guides/vortex_core_spectroscopy/postprocess_vortex_spectroscopy.json"

    # Run simulations.
    python superconga.py --file $CONFIG_SWAVE -S $DATA_PATH_SWAVE --no-visualize
    python superconga.py --file $CONFIG_DWAVE -S $DATA_PATH_DWAVE --no-visualize

    # Compute LDOS.
    python postprocess.py --file $POST_PROCESS_CONFIG --load-path $DATA_PATH_SWAVE
    python postprocess.py --file $POST_PROCESS_CONFIG --load-path $DATA_PATH_DWAVE

This will run multiple simulations of an Abrikosov vortex in superconducting discs with diameter :math:`\mathcal{D}=50\xi_0`, with *s*-wave and *d*-wave pairing symmetries. **Note:** These simulations might take a long time, since they are run with high spatial resolution to avoid artefacts in the vortex core LDOS, see the `LDOS tutorial`_ for more information.

The following shows the contents for the *d*-wave configuration file:

.. code-block:: bash
   :caption: examples/guides/vortex_core_spectroscopy/swave_vortex.json
   :name: config-file-vortex-core-spectroscopy-swave

    {
        "physics": {
            "temperature": 0.5,
            "external_flux_quanta": 1.5,
            "penetration_depth": 8.0,
            "crystal_axes_rotation": 0.0,
            "gauge_solenoid": false,
            "order_parameter": {
                "s": {
                    "critical_temperature": 1.0,
                    "initial_phase_shift": 0.0,
                    "initial_phase_winding": 1.0,
                    "initial_noise_stddev": 0.0
                }
            }
        },
        "geometry": [
            {
                "disc": {
                    "add": true,
                    "diameter": 50.0,
                    "center_x": 0.0,
                    "center_y": 0.0
                }
            }
        ],
        "accuracy": {
            "convergence_criterion": 1e-05,
            "energy_cutoff": 16.0,
            "num_fermi_momenta": 32,
            "num_iterations_burnin": 0,
            "num_iterations_max": 1000,
            "num_iterations_min": 0,
            "points_per_coherence_length": 20.0,
            "vector_potential_error": 1e-06
        },
        "accelerator": {
            "adaptive": true,
            "anderson_always": true,
            "inertia": 0.0,
            "max_mixing": 1.0,
            "min_mixing": 1.0,
            "rank": 1,
            "replace_oldest": true,
            "step_size": 1.0
        },
        "misc": {
            "data_format": "h5",
            "load_path": "",
            "save_frequency": -1,
            "save_path": "data/examples/vortex_core_spectroscopy/swave",
            "verbose": true,
            "visualize": true
        }
    }

After running the simulations and producing the data with the shell-scripts above, the following python script can be run (from the root directory):

.. _`LDOS tutorial`: ../tutorials.html#lesson-4-density-of-states-interactive-spectroscopy

.. code-block:: bash
   :caption: Generating the vortex-core spectroscopy plot.
   :name: plotting-vortex-core-spectroscopy

   python3 examples/guides/vortex_core_spectroscopy/plot_vortex_core_spectroscopy.py

which should generate the following plot:

.. figure:: ../_static/images/guides/vortex_core_spectroscopy.png

    Figure: ...

The LDOS can also be inspected interactively by running the following:

.. code-block:: bash
   :caption: Interactive spectroscopy of the vortex cores.
   :name: interactive-ldos-vortex-core

   python3 ldos_plotter.py --file examples/guides/vortex_core_spectroscopy/swave_vortex.json
   python3 ldos_plotter.py --file examples/guides/vortex_core_spectroscopy/dwave_vortex.json

for the *s*-wave and *d*-wave, respectively. It should look something like the following screenshot:

----

References
==========

.. footbibliography::
