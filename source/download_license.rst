SuperConga and its source code is free to |download from GitLab|, and is under the GNU LGPL v3 license or higher.

.. |download from GitLab| raw:: html

   <a href="https://gitlab.com/superconga/superconga" target="_blank">download from GitLab</a>
