================
 Tutorials
================
.. role:: bash(code)
   :language: bash

This page contains tutorials in the form of small lessons showing how to use and understand the various features of SuperConga. In contrast, the `how-to guides`_ show some research workflow and how to study specific physics scenarios. In general, simulation settings can be specified both via a config file, and command-line arguments. The tutorials focus mainly on the latter, since the former is explained in detail in the `technical reference`_.

- **Version:** The tutorials are based on SuperConga v1.0.
- **Pre-requisites:** The tutorials assume that all |dependencies have been installed|, and that SuperConga has been properly |built and and compiled|.

.. _`how-to guides`: guides/guides.html

.. |dependencies have been installed| raw:: html

    <a href="https://gitlab.com/superconga/superconga#installing-dependencies" target="_blank">dependencies have been installed</a>

.. |built and and compiled| raw:: html

    <a href="https://gitlab.com/superconga/superconga#setup-and-compilation" target="_blank">built and and compiled</a>

----

Lesson 1: Running simulations
=============================
This tutorial teaches basic usage of SuperConga via the Abrikosov-vortex example. It demonstrates how to run simulations from the existing configuration files in the SuperConga repository, and how to understand the resulting terminal output and live-visualization.

.. admonition:: Lesson 1

  - **File:** :bash:`examples/swave_disc_vortex/simulation_config.json`
  - **Learning Objectives:**

     - running simulations via :bash:`superconga.py simulate`
     - understanding terminal output
     - understanding live visualization


1.1. Running the Abrikosov-vortex example
-----------------------------------------
Starting from the root of the SuperConga repository, the Abrikosov-vortex example can be run via the following command:

.. code-block:: bash
   :caption: **Example: Abrikosov vortex in a disc-shaped s-wave grain**
   :name: example-swave-vortex

   python superconga.py simulate -C examples/swave_disc_vortex/simulation_config.json -T 0.5

Here, :bash:`simulate` is a subcommand to superconga.py, used to set up self-consistency simulations. A list of available subcommands can be obtained with :bash:`python superconga.py --help`, and the specific help for each subcommand is found via :bash:`python superconga.py <subcommand> --help`. In the above example, the argument :bash:`-C|--config` is used to provide the path to a :bash:`.json`-configuration file (alternatively, only the directory can be specified, if it contains a file named exactly :bash:`simulation_config.json`). The configuration file should specify all necessary parameters for running a self-consistency simulation. In this case, it specifies an *s*-wave superconducting disc with radius :math:`25 \xi_0`, at temperature :math:`T=0.5T_{\mathrm{c}}`, and exposed to the external magnetic flux :math:`\Phi_{\mathrm{ext}} = 0.5\Phi_0` (spread uniformly over the disc) and with Ginzburg-Landau coefficient :math:`\kappa=5`.

.. note::
    To run on a specific GPU on your machine, make sure to run :bash:`export CUDA_VISIBLE_DEVICES=X` in your terminal/shell script first, where :bash:`X` is the GPU ID (e.g. :bash:`0` as obtained by running :bash:`nvidia-smi`).

It is the task of the Python frontend to parse and validate the parameter file and command-line arguments, and if valid, sending them to the binary (e.g. :bash:`build/release/bin/superconga` for the release version). The Python frontend consists of the run-script :bash:`superconga.py`, and the Python library in :bash:`frontend/` which implements the actual functionality.

The binary will run the simulation until either reaching self-consistency within the convergence criterion, or the maximum number of iterations. Both of these are specified in the config file. Note that command-line arguments have a higher priority than the config file: in this case, the :bash:`-T` argument specifies to run at a temperature :math:`T=0.5T_{\mathrm{c}}`. The usage of both :bash:`.json`-files and command-line arguments makes it very convenient to set up a series of simulations, where the base parameters are set in the :bash:`.json`-file, while the uniquely varying parameters between simulations can be set through the command-line. This is a typical workflow used in the various research projects with SuperConga.

By default, the simulation will output the status of each iteration to terminal, and plot spatially resolved quantites (e.g. the order parameter) in a live-visualization window. The next two steps in the tutorial will look closer at this output and visualization.

.. note::
    - **Terminal output** is enabled with :bash:`-v|--verbose`, and disabled with :bash:`-q|--quiet`.
    - **Live visualization** is enabled with :bash:`--visualize`, and disabled with :bash:`--no-visualize`.
    - **Config-file path** is specified with :bash:`-C|--config`. If the file is called "simulation_config.json", then it can be omitted from the path, by specifying only the containing folder.

1.2. Terminal output from simulations
-------------------------------------
The Abrikosov-vortex example will result in a terminal output that looks similar to the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_terminal.png

    Figure: terminal output for the vortex example. Open image in new tab for a larger version.

The first line indicates the data output folder, and that the simulation configuration file has been saved to it (including any overriding command-line arguments), so that the simulation can be easily postprocessed. The next two lines indicate if there is any |burn-in| in the system (there is none in this case). In short, burn-in is a way to improve convergence when resuming a simulation: it keeps the order-parameter and vector potential constant for a few iterations (since these are often converged upon saving) and updates the coherence functions until they are also converged (they are not saved to file due to being disk-space intensive).

Next, the first column shows the iteration number of the self-consistent simulation. The second column shows the elapsed time in "hours:minutes:seconds". The third column shows how fast the code runs, in terms of how many self-consistent iterations are completed per second. This number will vary depending on e.g. the |discrete resolution| (in spatial space, momentum space, and energy), as well as which hardware and environment the simulation is run with. The fourth column shows the |maximum residual| of the order parameter in the grain, which is one possible measure of convergence. The simulation ends either when the |maximum number of iterations| has been reached, or the maximum residual drops below the |convergence criterion|. Both of these are set in the config file, and can be overridden by command-line arguments :bash:`-M|--num-iterations-max` and :bash:`-c|--convergence-criterion`, respectively. The fifth column in the terminal output shows the area-averaged free-energy difference between the superconducting and normal states. A negative energy therefore signifies that the superconducting phase is energetically favorable.

.. note::
    - **Convergence criterion** is set with :bash:`-c|--convergence-criterion`.
    - **The norm** used for the convergence criterion is set with :bash:`-n|--norm`.
    - **Maximum number of iterations** is set with :bash:`-M|--num-iterations-max`.
    - **Minimum number of iterations** is set with :bash:`-m|--num-iterations-min`.

.. |burn-in| raw:: html

   <a href="technical_reference.html#burn-in" target="_blank">burn-in</a>

.. |maximum residual| raw:: html

   <a href="technical_reference.html#maximum-residual" target="_blank">maximum residual</a>

.. |maximum number of iterations| raw:: html

   <a href="technical_reference.html#maximum-number-of-iterations" target="_blank">maximum number of iterations</a>

.. |convergence criterion| raw:: html

   <a href="technical_reference.html#convergence-criterion" target="_blank">convergence criterion</a>

.. |discrete resolution| raw:: html

   <a href="technical_reference.html#discrete-resolution" target="_blank">discrete resolution</a>
   
1.3. Live-visualization during simulations
------------------------------------------
By default, live visualization is enabled, and for the vortex example it will look something like the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_visualization.png

    Figure: Live visualization window of the Abrikosov-vortex example. Open image in new tab for a larger version.

.. note::
   Please note that the main point of the live visualization is not to do careful data analysis, but rather to get an overview of the progress of the simulation. For careful analysis, see instead the following tutorials on how to plot from converged data, via :bash:`python superconga.py plot-simulation`.

Here, the window shows heatmaps of various quantities and observables, as indicated by the text above each square. Specifically, this text consists of a label denoting the observable (e.g. the :math:`x`-component of the induced vector potential, :math:`A_x`), and numbers denoting the scale of the heatmap. Hence, the colors can also denote the extent of the superconducting grain (a disc in this case) against a dark-gray background (vacuum), while the full square denotes the simulation space. Please note that two different colormaps are used:

- the "sequential colormap" :bash:`YlGnBu`, where yellow/white denotes zero, and blue denotes the maximum value,
- the "diverging colormap" :bash:`RdBu`, where red denotes negative values, white denotes zero, and blue denotes positive values.

The first three panels in the first row use the colormap :bash:`YlGnBu`, while all others use the colormap :bash:`RdBu`. The left column shows the order-parameter amplitude (in units of :math:`2\pi k_{\mathrm{B}}T_{\mathrm{c}}`) and phase (from :math:`-\pi` to :math:`\pi`). The second column from the left shows the magnitude, x-component, and y-component, of the total current density, in units of :math:`j_0 \equiv 2\pi |e|v_{\mathrm{F}} N_{\mathrm{F}} k_{\mathrm{B}}T_{\mathrm{c}}`. In this example, there are Meissner screening currents along the perimeter of the disc. The third column shows the amplitude, x-component, and y-component, of the induced vector potential, in units of :math:`A_0 \equiv \Phi_0 / \pi \xi_0`. The fourth and final column shows the induced magnetic flux density, in units of :math:`B_0 \equiv \Phi_0 / \pi \xi_0^2`. For further explanation of the units, see e.g. the appendix of the SuperConga v1.0 paper.

----

Lesson 2: Saving and loading data from file
===========================================
By default, data is always saved to disk at the end of simulation. This tutorial shows how to control when and where data is saved, and in which format. The tutorial also specifies exactly which files are saved, and what their contents are. Finally, it is shown how data can be loaded to continue a simulation, possibly with changed parameters.

.. admonition:: Lesson 2

  - **File:** :bash:`examples/swave_disc_vortex/simulation_config.json`
  - **Learning Objectives:**

     - controlling how data is saved
     - converting data between csv and h5
     - understanding the data files
     - loading data and resuming simulations


2.1. Controlling how data is saved
----------------------------------
The field :bash:`"save_path"` in the config file determines the folder to which data is saved. Alternatively, this can also be changed via the command-line argument :bash:`-S|--save-path`. By default, data is saved to file at the end of each simulation. Note that when running the simulation with live visualization, the simulation needs to be closed in a controlled manner for data to be saved to file, i.e. by pressing "escape" or the "X" in the window (as opposed to closing it from terminal via "CTRL^C").

Data can also be saved during regular intervals. This behavior is controlled with either the command-line argument :bash:`-F|--save-frequency`, or in the config file via the field :bash:`"save_frequency"`. A positive integer indicates the frequency with which data is saved during the simulations (and that data will be saved at the end of the simulation), a negative value indicates that data is only saved at the end of the simulation, while a value of zero means that data will not be saved at all. The following command shows how to save data every 50th iteration during the vortex example (starting from the root directory):

.. code-block:: bash
   :caption: **Example: saving data to disk**
   :name: example-swave-vortex-save-data

   python superconga.py simulate -C examples/swave_disc_vortex/ -F 50

When data is saved, it will generate terminal output similar to the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_save_data.png

    Figure: terminal output from saving data to file.

Here, :bash:`.h5` is short for :bash:`hdf5`, which is a highly compressed data format. It is generally not human-readable, but is widely supported in different languages and tools. Alternatively, the data format can be changed to a human-readable :bash:`.csv`-file (Comma-Separated Values) in which data is saved in comma-separated columns with explicit column labels. This is done via the command-line argument :bash:`-D|--data-format`.

Please note that data can be converted from :bash:`.csv` to :bash:`.h5` (or vice-versa) via the subcommand :bash:`python superconga.py convert`, see its corresponding help message with the flag :bash:`--help`.

2.2. Data file explanation
--------------------------
When using the default data format :bash:`.h5`, the following files are generated during a regular simulation:

- **simulation_results.json** - Contains scalar results, in the form of area-averaged quantities:

    - *total current density* (in units of :math:`j_0 \equiv 2\pi |e|v_{\mathrm{F}} N_{\mathrm{F}} k_{\mathrm{B}}T_{\mathrm{c}}`),
    - *induced magnetic flux density* (in units of :math:`B_0 \equiv \Phi_0 / \pi\xi_0^2`),
    - *free energy* (in units of :math:`\Omega_{\mathcal{A}} \equiv (2\pi k_{\mathrm{B}}T_{\mathrm{c}})^2N_{\mathrm{F}}\mathcal{A}`),
    - *magnetization energy* (in units of :math:`\Omega_{\mathcal{A}}`),
    - *magnetic moment* (in units of :math:`m_0 \equiv j_0\xi_0\mathcal{V}`),
    - *order-parameter magnitude* (in units of :math:`2\pi k_{\mathrm{B}}T_{\mathrm{c}}`),
    - *induced vector potential* (in units of :math:`A_0 \equiv \Phi_0 / \pi\xi_0`).

- **internal_parameters.json** - various parameters calculated and used internally by the framework (e.g. the number of discrete points in the superconducting grain).
- **simulation.h5** - contains spatially dependent quantities and observables (e.g. order parameter and current density).
- **simulation.pdf** - A plot of the spatially dependent quantities, equivalent to that produced with :bash:`python superconga.py plot-simulation`. See the corresponding tutorial further down for details.
- **simulation_convergence.pdf** - Plot of various residuals versus iteration, and the free energy versus iteration number. Equivalent to that produced with :bash:`python superconga.py plot-convergence`. See the corresponding tutorial further down for details.
- **simulation_convergence.csv** - Contains the residuals and free energy for each iteration.
- **simulation_config.json** - The configuration settings used during the simulation. Saved at the start of the simulation.

If using :bash:`.csv` instead, i.e. via :bash:`python superconga.py simulate -D csv ...`, then the spatially resolved quantities from :bash:`simulation.h5` will instead be saved to the following separate files:

- **domain.csv** - a mask containing the discrete representation of the superconducting grain, with 0 and 1 denoting points outside and inside the geometry. Note that the exact boundary and boundary normals are not saved in this file, but instead generated internally in the program from the geometry description (the latter provided via config file or command-line).
- **order_parameter.csv** - real and imaginary part for every order-parameter component.
- **current_density.csv** - x- and y-component of the total current density.
- **vector_potential.csv** - x- and y-component of the induced vector potential.
- **magnetic_flux_density.csv** - the induced magnetic flux density.

As a specific example, :bash:`current_density.csv` has columns for the discrete x- and y-coordinates (called :bash:`x_idx` and :bash:`y_idx`), and columns for the x- and y-components of the current (called :bash:`j_x` and :bash:`j_y`).


2.3. Loading data and resuming simulations
------------------------------------------
Assuming that data was saved to file, as in the above examples, it is straightforward to load the data to resume the simulation. This is done via the command-line argument :bash:`-L|--load-path` followed by the relative path to the data, together with the argument :bash:`-C|--config` followed by the path to the simulation configuration. The former reads the order-parameter and vector-potential data, while the latter is important to specify which settings to use when resuming the simulation. It is useful to separate the config and data like this, since the user can then choose to either resume the simulations with exactly the same parameters, or with new updated ones. We illustrate both of these approaches below.

Resuming with the same parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
First, we want to resume a simulation based on the example :bash:`swave_disc_vortex`. Since the simulation always saves the configuration file together with the other data, we can ensure that we run with the exact same parameters by specifying the same config path and load path:

.. code-block:: bash
   :caption: **Resuming the Abrikosov-vortex simulation**
   :name: example-swave-vortex-continue

   python superconga.py simulate -L data/examples/swave_disc_vortex/ -C data/examples/swave_disc_vortex/

It is important to note that while the order-parameter and vector-potential are both saved to file by default, the coherence functions are not since they are generally too disk-space intensive. For this reason, special care has to be taken when resuming a simulation, and SuperConga allows for a "burn-in" period during which the order-parameter and vector potential will be kept constant, until the coherence functions (especially at the boundary) can reach self-consistency again. The number of iterations to do a burn-in is specified via the argument :bash:`-b|--num-iterations-burnin`. Zero means no burn-in, and a negative value means running until reaching self-consistency (within the specified tolerance). The latter is illustrated below:

.. code-block:: bash
   :caption: **Resuming the Abrikosov-vortex simulation: with burn-in**
   :name: example-swave-vortex-continue-burnin

   python superconga.py simulate -b -1 -L data/examples/swave_disc_vortex/ -C data/examples/swave_disc_vortex/

which should give an output similar to the following image:

.. figure:: _static/images/tutorials/burnin_example.png

    Figure: burn-in when resuming a simulation.

Here, we see that it takes roughly 20 iterations for the boundary to reach a self-consistency below :math:`10^{-5}`. Once the burn-in is finished, the order parameter and vector potential are no longer fixed, but allowed to be changed in the self-consistency iteration. It is seen that it takes only one iteration for everything to reach convergence after the burn-in.

Resuming with modified parameters
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Now, we show how an old simulation can be used as a "start guess" for a new simulation with different parameters. We again use the example of the Abrikosov vortex, but change the temperature to :math:`T=0.6T_{\mathrm{c}}`:

.. code-block:: bash
   :caption: **Resuming the Abrikosov-vortex simulation: with another temperature**
   :name: example-swave-vortex-new-parameter

   python superconga.py simulate -b -1 -T 0.6 -L data/examples/swave_disc_vortex/ -C data/examples/swave_disc_vortex/ -S data/examples/swave_disc_vortex/T0-6/

In particular, we see that we change the save path, since we do not want to overwrite the original data.

More in general, the concept of resuming simulations is the starting point for "parameter sweeps", where multiple simulations are run in succession, but with varied parameters in each simulation. This is useful for example to calculate phase diagrams, and is illustrated in detail in later tutorials, and in :bash:`examples/parameter_sweeps/` in the repository.

----

Lesson 3: Visualizing simulation data
=====================================
This tutorial explains how to use existing tools to analyze and plot the data generated from self-consistency simulations. In particular, we will look at the two subcommands :bash:`plot-simulation` and :bash:`plot-convergence`, used for plotting spatially dependent quantities and convergence data, respectively. The spatially dependent quantities are the order parameter, charge-current density, induced vector potential, and induced magnetic flux density. For the LDOS, see the other tutorials below.

.. admonition:: Lesson 3

  - **File:** :bash:`examples/swave_abrikosov_lattice/simulation_config.json`
  - **Learning Objectives:**

     - plotting spatial quantities with :bash:`python superconga.py plot-simulation`
     - visualizing convergence status with :bash:`python superconga.py plot-convergence`


3.1. Plotting spatial quantities from file
------------------------------------------
Assume that the following simulation is run and that data is saved to disk successfully:

.. code-block:: bash
   :caption: **Running the Abrikosov-vortex lattice example**
   :name: example-swave-vortex-lattice-example

   python superconga.py simulate -C examples/swave_abrikosov_lattice/ -S data/vortex_lattice/

The spatially dependent quantities from the above simulation can be visualized with the :bash:`plot-simulation` subcommand, by specifying the location of the data with the command-line argument :bash:`-L|--load-path`:

.. code-block:: bash
   :caption: **Plotting quantities from raw-data file**
   :name: example-swave-vortex-lattice-plot-data

   python superconga.py plot-simulation -L data/vortex_lattice/

This will generate a plot similar to the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_lattice_plot_data.png

    Figure: Abrikosov-vortex lattice example, plotted from raw-data file.

Here, each heatmap is properly labeled and quantified (as opposed to the restricted live-visualization). The units are defined in the appendix of the SuperConga v1.0 paper. Note that :math:`\angle \mathbf{j}` is the "angle" of the charge-current density, defining the direction of current circulation. Together with the magnitude, it uniquely defines the charge-current density :math:`\mathbf{j}`. The command-line flag :bash:`--cartesian` can be used to instead plot the x- and y-components, as well as the real and imaginary parts of the order parameter (as opposed to the magnitude and phase):


.. code-block:: bash
   :caption: **Plotting quantities from raw-data file (cartesian)**
   :name: example-swave-vortex-lattice-plot-data-cartesian

   python superconga.py plot-simulation --cartesian -L data/vortex_lattice/

.. figure:: _static/images/tutorials/swave_disc_vortex_lattice_plot_data_cartesian.png

    Figure: Abrikosov-vortex lattice example, plotted from raw-data file (cartesian).

Note that the command-line argument :bash:`-S|--save-path` can be added to :bash:`plot-simulation` to save the plot to file, rather than plotting to window. The file type can also be specified (e.g. PNG or PDF), see the help :bash:`python superconga.py plot-simulation --help`.

Which fields are plotted can be controlled with the command-line argument :bash:`--view {D,J,A,B}`, where the letters denote the order-parameter (D), charge-current density (J), induced vector potential (A), and induced magnetic-flux density (B). Hence, to plot only the current and magnetic-flux density, use :bash:`--view {J,B}`.

Additional command-line arguments exist to control the size of the subplots, the text properties, as well as the colors/colormaps. See the help :bash:`python superconga.py plot-simulation --help`.

3.2. Visualizing the convergence status
---------------------------------------
The subcommand :bash:`plot-convergence` has been added to enable analysis of the iteration status and convergence. Following the previous step of this tutorial, the convergence plot for the Abrikosov-vortex lattice example can be generated via:

.. code-block:: bash
   :caption: **Plotting convergence status**
   :name: example-swave-vortex-lattice-plot-convergence

   python superconga.py plot-convergence -L data/vortex_lattice/

This will generate a plot similar to the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_lattice_plot_convergence.png

    Figure: Convergence plot for the Abrikosov-vortex lattice example. Open image in new tab for larger version.

Panel (a) plots the residual versus iteration number, for the order parameter (:math:`\Delta`), charge-current density (:math:`\mathbf{j}`), induced vector potential (:math:`\mathbf{A}`), free energy (:math:`\Omega`), and the coherence functions at the boundary (:math:`\gamma_{\partial \mathcal{D}}`). Apart from the vector potential, the latter can be one of the most difficult/crucial quantities to converge. The residual is defined via one of the :math:`\{L_1,L_2,L_\infty\}` norms, as specified by the argument :bash:`-n|--norm` when running the simulation. A horizontal dotted line has been added to the plot to indicate the convergence criterion used during the simulation. Panel (b) shows the free energy relative to the smallest free energy found, versus iteration.

As with the other subcommands, :bash:`plot-convergence` comes with a number of arguments that can be used to control the behavior of the plot, see the help :bash:`python superconga.py plot-convergence --help`. For example, the plot can be saved directly to file (as opposed to plotted to window) via :bash:`-S|--save-path`, and a Gaussian blur can be added to smooth out particularly spiky curves. The following figure shows the convergence plot for the example :bash:`examples/dwave_phase_crystal` with :bash:`--blur 5`:

.. figure:: _static/images/tutorials/dwave_phase_crystal_plot_convergence_blur.png

    Figure: Convergence plot for the phase crystal example, with Gaussian blur.

The following figure shows the same convergence plot, but with :bash:`--blur 0` (default), compare in particular the black solid lines:

.. figure:: _static/images/tutorials/dwave_phase_crystal_plot_convergence.png

    Figure: Convergence plot for the phase crystal example, without any blur.


----

Lesson 4: Customizing simulations and general workflow
======================================================
This short tutorial illustrates how to set up custom simulations and parameter sweeps, via a combination of customized configuration files, command-line arguments, and shell-script files. It also gives some general advice on workflow.

.. admonition:: Lesson 4

  - **File:** :bash:`examples/swave_disc_meissner/simulation_config.json`
  - **Learning Objectives:**

     - modifying configuration files
     - command-line arguments
     - running parameter sweeps from shell-scripts


4.1. General workflow advice
----------------------------
The simulation settings can either be modified directly in the config :bash:`.json`-files, or via command-line argument. A recommended workflow is to not to modify the existing configuration files in the SuperConga repository, but instead use them as templates for new config files, the latter placed e.g. in :bash:`private/`. The reason for this is that :bash:`git` tries to track any changes made to the SuperConga files, making subsequent updates and other git operations messy. In contrast, the :bash:`private/` folder is ignored by git (as specified in the file :bash:`.gitignore`).

Another general suggestion when doing many similar simulations, is to keep the configuration files are static as possible by using them to define the base setup, and then use command-line arguments to specify the unique values for each simulation. This reduces the file clutter, and is possible since the command-line arguments have the highest priority, overriding the values in the configuration files.

.. note::
    Command-line has the highest priority, overriding the values in the :bash:`.json`-configuration files.

4.2. Custom simulations: modifying the Meissner-state example
-------------------------------------------------------------
The following example shows how to use command-line arguments to set the external flux to :math:`\Phi=1.0\Phi_0`, the temperature to :math:`T=0.05T_{\mathrm{c}}`, the Penetration depth to :math:`\lambda_0 = 4\xi_0`, the data output folder to :bash:`data/examples/dwave_disc_meissner`, and the order-parameter symmetry to :math:`d_{x^2-y^2}`:

.. code-block:: bash
   :caption: **Changing the Meissner-state example via command-line arguments**
   :name: example-swave-meissner-cli

   python superconga.py simulate -C examples/swave_disc_meissner -S data/dwave_disc_meissner -B 1 -T 0.05 -k 4 --dx2-y2-wave 1 0 0 --no-s-wave -N 10 -p 64

Here, :bash:`--dx2-y2-wave 1 0 0 --no-s-wave` removes the :math:`s`-wave component added by the config file, and adds the :math:`d_{x^2-y^2}`-wave component with relative :math:`T_{\mathrm{c}}`, with zero initial phase winding  and noise. The :math:`s`-wave component can of course be kept to simulate e.g. a :math:`d+is` superconductor (by setting the relative :math:`T_{\mathrm{c}}` lower for the :math:`s`-wave). See the tutorial on customizing the order-parameter symmetry for more information. The arguments :bash:`-N 10 -p 64` have been added to better capture the mesoscopic effects, by setting the discrete points per coherence length to 10, and the number of discrete Fermi momenta to 64.

Using the configuration file of the Meissner-state example as a template, the geometry can then be modified into a hexagon with side-length :math:`10 \xi_0`, by changing the geometry entry to the following:

.. code-block:: bash
   :caption: **Changing the Meissner-state example to a hexagon geometry**
   :name: example-swave-meissner-config

        "geometry": [
            {   
                "regular_polygon": {
                    "add": true,
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "num_edges": 6,
                    "rotation": 0.0,
                    "side_length": 10.0
                }
            }
        ],

After successfully running a simulation with the above settings and saving data to file in `data/dwave_hexagon_meissner`, the results can be visualized via:

.. code-block:: bash
   :caption: **Plotting the custom d-wave Meissner example**
   :name: example-dwave-meissner-plot

   python superconga.py plot-simulation -L data/dwave_hexagon_meissner

which should generate a figure similar to the following:

.. figure:: _static/images/tutorials/dwave_meissner_custom_plot_data.png

    Figure: Visualization of results from customized *d*-wave Meissner simulation.

These results show a few notable features, in particular the suppression of the order parameter due to pair-breaking Andreev reflection at the :math:`[110]`-interfaces at the top and bottom. This pair-breaking leads to quasiparticles with paramagnetic response (at the top/bottom interfaces), while the condensate has the usual diamagnetic response (at the left/right interfaces and in the central region).

4.3. Sweeping a parameter in the Meissner-state example
-------------------------------------------------------
Since the command-line arguments override the values of the configuration file, parameter sweeps can be conveniently set up e.g. via shell scripts or workflow tools such as snakemake. The shell-script file below shows how to sweep the external flux between :math:`\Phi_{\mathrm{ext}}=0\Phi_0` and :math:`\Phi_{\mathrm{ext}}=5\Phi_0` in steps of :math:`\Delta\Phi_{\mathrm{ext}} = 0.5 \Phi_0`, in the Meissner-state example. This should cover the whole Meissner phase, since the highest flux leads to a flux density above the first critical field :math:`B_{\mathrm{c}1}` for Abrikosov-vortex entry. The flag :bash:`--no-visualize` specializes that the simulations should be run in terminal-only mode, appropriate e.g. over SSH and in cluster environments:

.. code-block:: bash
   :caption: **meissner_sweep.sh**
   :name: example-swave-meissner-sweep

   #!/bin/bash
   # meissner_sweep.sh - sweep the external flux in the Meissner state.

   # Path to base configuration file.
   CONFIG_PATH="examples/swave_disc_meissner/simultation_config.json"
   
   # Base path of data output.
   DATA_BASE_PATH="data/meissner_sweep"
   
   # Loop over flux in steps of 0.5 flux quanta.
   FLUX_MIN=0
   FLUX_MAX=5
   FLUX_STEP=0.5
   for FLUX in $(seq $FLUX_MIN $FLUX_STEP $FLUX_MAX); do
       # Create unique data path for each simulation.
       DATA_PATH=`printf "$DATA_BASE_PATH/phi_%2.1f" $FLUX`

       # Run simulation with unique flux value.
       python superconga.py simulate -C $CONFIG_PATH -S $DATA_PATH --no-visualize -B $FLUX
   done

Running this shell script will save data from each simulation in a unique sub-folder in :bash:`data/meissner_sweep/`, which can then be postprocessed and analyzed.

----

Lesson 5: Density of states & interactive spectroscopy
======================================================
This tutorial shows how to compute the density of states (DOS) and local density of states (LDOS) with :bash:`python superconga.py postprocess`, and how to plot these quantities using an interactive spectroscopy tool :bash:`python superconga.py plot-postprocess`. The term "postprocess" is used since the DOS and LDOS are computed in a postprocessing step, from data generated by self-consistency simulations. The reason for this is that the DOS is computationally expensive, since it is calculated on the real axis using causal propagators (Retarded in time), as opposed to as a sum over poles on the imaginary axis using Matsubara propagators. Hence, additional self-consistency simulations have to be ran to obtain these propagators, and these simulations generally require a much higher number of discrete energies, spatial resolution and number of Fermi momenta.

.. admonition:: Lesson 5

  - **Files:** :bash:`examples/swave_disc_vortex/simulation_config.json` and :bash:`examples/swave_disc_vortex/postprocess_config.json`
  - **Learning Objectives:**

     - computing the DOS and LDOS via :bash:`python superconga.py postprocess`
     - interactive spectroscopy via :bash:`python superconga.py plot-postprocess`

.. warning::

    Accurately computing the density of states generally relies on having a very high resolution, both spatially and on the Fermi surface. The number of Fermi momenta typically needs to be between :math:`N_\theta = 700` and :math:`N_\theta = 1000`, and the spatial resolution close to :math:`N=20` points per coherence lengths. This is especially true for phenomena varying on the coherence length scale (like vortex cores). Not using an appropriate resolution will lead to nonsense results and numerical artefacts in the density of states. The high resolution can lead to large memory requirement and long computational times, which is the main reason the DOS calculation is done as a postprocessing step. If the memory runs out, one can try to reduce the number of energies per block in :bash:`postprocess_config.json`.

5.1. Computing the density of states
------------------------------------
As a first step, a self-consistency simulation has to be run, and the data has to be saved successfully to disk, i.e. via:

.. code-block:: bash
   :caption: **Example: Abrikosov vortex in a disc-shaped s-wave grain**
   :name: example-swave-vortex-2

   python superconga.py simulate -C examples/swave_disc_vortex/ -S data/vortex_dos/ -N 20

Here, the argument :bash:`-N 20` has been added to set the points per coherence length to 20, which is crucial to accurately capture the LDOS in the vortex core. The DOS and LDOS can then be computed via the subcommand :bash:`postprocessing`. The minimal running example requires that the user specifies the location of the data, as well as a valid DOS config, e.g. :bash:`examples/swave_disc_vortex/postprocess_config.json` (the file name can be omitted if it is exactly :bash:`postprocess_config.json`):

.. code-block:: bash
   :caption: **Example: Abrikosov vortex DOS and LDOS**
   :name: example-swave-vortex-dos

   python superconga.py postprocess -C examples/swave_disc_vortex/ -L data/vortex_dos/

Here, the frontend will parse the data in the load path (including the contents of :bash:`simulation_config.json`), as well as :bash:`postprocess_config.json`. Every example in SuperConga comes with such a config file, below follows an example:

.. code-block:: bash
   :caption: **examples/swave_disc_vortex/postprocess_config.json**
   :name: example-ldos-postprocess-json

    {
        "spectroscopy": {
            "energy_max": 0.5,
            "energy_min": 0.0,
            "energy_broadening": 0.008,
            "num_energies": 128
        },
        "numerics": {
            "convergence_criterion": 1e-4,
            "norm": "l2",
            "num_energies_per_block": 32,
            "num_fermi_momenta": 720,
            "num_iterations_burnin": -1,
            "num_iterations_max": 1000,
            "num_iterations_min": 0
        },
        "misc": {
            "data_format": "h5",
            "load_path": "data/examples/swave_disc_vortex",
            "save_path": "",
            "verbose": true
        }
    }

The group :bash`"spectroscopy"` defines the energy scale and resolution for the DOS (which is then mirrored to negative energies, assuming particle-hole symmetry). The :bash:`"numerics"` group contains settings influencing the numerical accuracy during the new self-consistent simulations, and :bash:`"misc"` contains all remaining settings. For a more detailed explanation of the postprocess-config file, see the `technical reference`_. Please note that these settings can also be specified and overridden via command-line, see :bash:`python superconga.py postprocess --help`.

Running the DOS calculation will generate terminal output similar to the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_ldos.png

    Figure: terminal output for the postprocessing simulation. Open image in new tab for a larger version.

Here, we begin by noting that SuperConga reads the order parameter and vector potential from file (as well as the simulation config, which is not indicated in the figure). Second, we point out that the application of the burn-in, during which the already converged order parameter and vector potential are kept constant, until the causal propagators are computed and the coherence functions (in particular at the boundary) have reached self-consistency. In the above example, the burn-in takes 22 iterations. After the burn-in is completed, all quantities are updated self-consistently, and the simulation converges in only 2 iterations. Third, we note that the simulation runs in several blocks/stages (four blocks to be specific, in this case), to reduce the memory consumption. This means that the simulations take longer, since the amount of parallelization (on the GPU) is reduced correspondingly. The reason for this is that DOS and LDOS calculations are very memory-intensive, and consumer-grade GPUs are relatively limited in memory, as compared to typical RAM sizes. GPUs designed for high-performance computing and clusters are less restrictive and can run with a higher degree of parallelization. At the end of the postprocessing simulation, data will be saved to file, and the terminal output will be similar to the following image:

.. figure:: _static/images/tutorials/swave_disc_vortex_ldos_save_data.png

    Figure: terminal output from saving data to file from the postprocessing simulation.

We see that quite a large number of iterations have been run to reach convergence, as compared with the regular self-consistency simulation. This is again related to the DOS being relatively expensive to calculate. The following files are saved to disk:

- **postprocess_config.json** - contains the postprocessing settings.
- **postprocess.h5** - contains the LDOS data. The data format can be changed to :bash:`.csv` via :bash:`-D|--data-format csv`, but be warned that it can be an order of magnitude larger in disk-space.
- **postprocess_convergence.pdf** - a plot of the convergence of the postprocessing simulation, equivalent to that produced via :bash:`python superconga.py plot-convergence --view postprocess` as described in the next step of the tutorial.

By default, the postprocessing data is saved to the same folder where the original data is located, but it can be changed via the argument :bash:`-S|--save-path`. The following step of the tutorial shows how to visualize the DOS, LDOS and the postprocessing-convergence.

5.2. Interactive spectroscopy
-----------------------------
Assuming that the density of states was calculated and saved to disk (as in the previous step of the tutorial),  it can be visualized using the interactive plotting tool with the subcommand :bash:`plot-postprocess`. The user has to specify the path to the data using the argument :bash:`-L|--load-data`:

.. code-block:: bash
   :caption: **Plotting DOS and LDOS interactively**
   :name: example-ldos-plotter

   python superconga.py plot-postprocess -L data/vortex_dos/

This brings up an interactive spectroscopy tool shown below: 

.. figure:: _static/images/tutorials/ldos_plotter.png

    Figure: screenshot of the interactive spectroscopy tool (LDOS plotter), showing the averaged spectrum of an Abrikosov vortex core. Open image in new tab for a larger version.

Panel (a) shows the LDOS as a heatmap versus coordinate at a fixed energy. This energy is indicated by the title, and the vertical dashed line in panel (b). The user can change this energy by clicking in panel (b). Panel (b) shows the area-averaged DOS as a black solid line, and the LDOS at the red cross in panel (a) as a red solid line. The user can change the location of the cross by clicking in panel (a).

5.3. Postprocessing convergence
-------------------------------
The convergence status for the DOS postprocessing can be visualized and analyzed similar to the regular self-consistency simulations. Following the previous steps of the simulation, it is run via:

.. code-block:: bash
   :caption: **Plotting convergence status for DOS postprocessing**
   :name: example-ldos-convergence

   python superconga.py plot-convergence -L data/vortex_dos/ --view postprocess

which will generate a plot similar to:

.. figure:: _static/images/tutorials/ldos_convergence.png

    Figure: convergence plot for the postprocessing simulation.

We note in particular the convergence of each energy block, denoted by the transients. Since the order parameter and vector potential are converged in the regular simulation (run prior to the postprocessing), it is the residuals of the coherence functions (at the boundaries) and the DOS which determine the convergence. The horizontal dotted line marks the convergence criterion used during the postprocessing.

----

Lesson 6: Order-parameter symmetry and start guess
==================================================
The paring symmetry and start guess of the order-parameter can be specified via both command-line argument, and in the :bash:`.json` config-file. This tutorial starts by describing these two methods separately, and then shows how to set up specific single-component order parameters, as well as subdominant/multi-component orders. It also shown how to modify the start guess by adding noise, a phase shift, and Abrikosov vortices at specific coordinates. Finally, three examples are considered to illustrate this functionality:

- Abrikosov vortices in a :math:`d+is` superconductor,
- spontaneous time-reversal symmetry-breaking in a :math:`d+ig` superconductor,
- chiral :math:`d`-wave superconductivity.

.. admonition:: Lesson 6

  - **Files:** :bash:`examples/.../simulation_config.json`
  - **Learning Objectives:**

     - command-line versus config file
     - **start guess:** noise, phase shift, vortices
     - simulating **single-component** orders, e.g. :math:`s`, :math:`d_{x^2-y^2}` 
     - adding a **subdominant order**, e.g. :math:`d+is`
     - simulating **chiral orders**, e.g. :math:`d_{xy}+id_{x^2-y^2}`

6.1. Setting the order-parameter via command line
-------------------------------------------------
Using command-line arguments, each order-parameter component can be added or removed with a corresponding argument. This is shown explicitly in the following example, which reads the config file from the Meissner-state example, but replaces the :math:`s`-wave with a relatively noisy :math:`d_{xy}`-wave:

.. code-block:: bash
   :caption: **Run the Meissner example, but with a noisy d-wave as start guess**
   :name: example-noisy-dwave

   python superconga.py simulate -C examples/swave_disc_meissner/ --no-s-wave --dxy-wave 1.0 0.0 2.0

Here, it is the argument :bash:`--no-<symmetry>-wave` which explicitly removes the component with pairing symmetry :bash:`<symmetry>` from the config file, and the argument :bash:`--<symmetry>-wave` which adds another component. The valid pairing symmetries are currently:

- :bash:`--s-wave` - :math:`A_{1g}(s)`, with basis function :math:`\eta(\varphi) = 1`,
- :bash:`--dx2-y2-wave` - :math:`B_{1g}(d_{x^2-y^2})`, with basis function :math:`\eta(\varphi) = \sqrt{2} \cos(2\varphi)`,
- :bash:`--dxy-wave` - :math:`B_{2g}(d_{xy})`, with basis function :math:`\eta(\varphi) = \sqrt{2} \sin(2\varphi)`,
- :bash:`--g-wave` - :math:`A_{2g}(g_{xy(x^2-y^2)})`, with basis function :math:`\eta(\varphi) = \sqrt{2} \sin(4\varphi)`,

where :math:`\varphi` is the polar angle on the Fermi surface. Please note that :bash:`--<symmetry>-wave` has to be followed by three numbers as in the example above, corresponding to the parameters: :bash:`<critical_temperature> <initial_phase_shift> <initial_noise_stddev>`, respectively. The parameter :bash:`<critical_temperature>` is a positive real number specifying the critical temperature relative to the dominant component, and is therefore only meaningful when multiple components are present. Upon starting the simulation, all critical temperatures will be re-normalized such that the dominant component has a critical temperature of 1.0 (i.e. :math:`\operatorname{max}\{T_{\mathrm{c,\Gamma}}\}`). The parameter :bash:`<initial_phase_shift>` is a real number denoting the phase shift of the start guess, in units of :math:`2\pi`. Finally, the parameter :bash:`<initial_noise_stddev>` denotes the standard-deviation of a random gaussian perturbation of the order-parameter start guess, in units of :math:`2\pi k_{\mathrm{B}}T_{\mathrm{c}}`. Please see the command-line help for more information on these parameters.

It is straightforward to extend the above to study multiple order parameters. The following shows how to study the Meissner response of a :math:`d+is` superconductor:

.. code-block:: bash
   :caption: **d-wave superconductor with a subdominant s-wave component**
   :name: example-subdominant-order

   python superconga.py simulate -C examples/swave_disc_meissner/ --dxy-wave 1 0 0 --s-wave 0.1 0 0 -S data/dwave_plus_swave_meissner

Here, the :math:`s`-wave component is subdominant with a :math:`T_{\mathrm{c}}` that is a tenth of the *d*-wave component.

.. note::
    Adding noise to the start guess can severely reduce the ability to reach self-consistency fast, but can be useful to test the stability of a particular configuration (e.g. giant vortices). To add noise to a partially/fully converged solution, see the later tutorial on adding noise.

6.2. Setting the order-parameter via config file
------------------------------------------------
The following shows the order-parameter section of the file :bash:`examples/swave_abrikosov_lattice/simulation_config.json`:

.. code-block:: bash
   :caption: **Order-parameter pairing-symmetry in the s-wave Abrikosov-vortex lattice example**
   :name: example-swave-abrikosov-lattice-op

    "order_parameter": {
        "s": {
            "critical_temperature": 1.0,
            "initial_phase_shift": 0.0,
            "initial_noise_stddev": 0.0,
            "vortices": [
                {
                    "center_x": 0.0,
                    "center_y": 0.0,
                    "winding_number": -13.0
                }
            ]
        }
    }

Here, :bash:`"order_parameter": {...}` is a dictionary, where each entry is a key (in this case :bash:`"s"`) which specifies a unique component of the total order parameter. The valid keys are :bash:`"s"`, :bash:`"dx2-y2"`,  :bash:`"dxy"`, and :bash:`"g"`, equivalent to the command-line options. All the settings for each component are equivalent to the command-line parameters, with the exception of :bash:`"vortices"`, as described in the example below. For more details on the :bash:`.json`-file fields, see the `technical reference on configuration/parameter files`_.

It is straightforward to add multiple order parameters, following the :math:`d+is` example in :bash:`examples/dwave_plus_swave/simulation_config.json`. The order-parameter section of this config is:

.. code-block:: bash
   :caption: **Order-parameter pairing-symmetry in config.json of the dwave_plus_swave example**
   :name: example-dwave-plus-swave-op-sym

    "order_parameter": {
        "dxy": {
            "critical_temperature": 1.0,
            "initial_phase_shift": 0.0,
            "initial_noise_stddev": 0.0,
            "vortices": []
        },
        "s": {
            "critical_temperature": 0.1,
            "initial_phase_shift": 0.0,
            "initial_noise_stddev": 0.0,
            "vortices": []
        }
    }

Here, the values of the fields :bash:`"critical_temperature"` indicate that the *s*-wave component is subdominant with a :math:`T_{\mathrm{c}}` that is a tenth of the *d*-wave component.

.. _`technical reference on configuration/parameter files`: technical_reference.html#configuration-parameter-files

6.3. Adding vortices to the start guess
---------------------------------------
In the order-parameter part of the config file, the field :bash:`"vortices"` is a list that enables the addition of vortices (and anti-vortices) to the start guess. Using a start-guess with specific vorticity can significantly speed up convergence. Furthermore, due to the Bean-Livingston barrier, it is possible to study not just the ground-state, but potentially one of many meta-stable states. Generally speaking, there can be multiple meta-stable states for each number of vortices in the system, corresponding to different geometric arrangements. In the configuration-file example above, a giant vortex with phase winding :math:`-13 \times 2\pi` is added at the coordinate :math:`(x,y) = (0,0) \xi_0`.

.. note::
    *Vortices* and *anti-vortices* are defined by the sign of the phase winding, with respect to the external magnetic field and charge carrier. The sign of the charge carrier is set by either :bash:`"charge_sign"` in the config, or the argument :bash:`--charge-sign`. Negative (positive) charge sign means that particle currents and charge currents are anti-parallel (parallel).
    
    **Example:** For negative charge sign (:math:`e = -|e|`) and a positive external magnetic field (:math:`\mathbf{B}_{\mathrm{ext}} = +|B_{\mathrm{ext}}|\hat{\mathbf{z}}`), vortices are defined via a negative phase winding, while anti-vortices have a positive phase winding.

When studying vortex lattices, it is usually faster (and safer) to start with multiple singly-quantized vortices instead of a giant vortex. Multiple vortices (and anti-vortices) can be added by extending the list with more entries, for example:

.. code-block:: bash
   :caption: **Adding multiple vortices via config file**
   :name: example-config-vortices

    "vortices": [
        {
            "center_x": 5.0,
            "center_y": 5.0,
            "winding_number": -1.0
        },
        {
            "center_x": 5.0,
            "center_y": -5.0,
            "winding_number": -1.0
        },
        {
            "center_x": -5.0,
            "center_y": -5.0,
            "winding_number": -1.0
        },
        {
            "center_x": -5.0,
            "center_y": 5.0,
            "winding_number": -1.0
        }
    ]

Be careful not to add a comma after the curly bracket for the final vortex, as the :bash:`.json`-file format is very picky with syntax.


To add vortices via command-line instead, the argument :bash:`--vortex` can be added multiple times, as in the example below. It must be followed by three real numbers, specifying the x-coordinate (in :math:`\xi_0`), y-coordinate (in :math:`\xi_0`), and phase winding (in :math:`2\pi`) of the vortex, respectively. See the command-line help :bash:`python superconga.py simulate --help` for more information.

.. note::
    The phase winding/Abrikosov vortex of the start guess will be applied to all order-parameter components.

6.4. Example: vortices in a d+is superconductor
-----------------------------------------------
The following shows how to add a number of vortices to a :math:`d+is` superconductor, with :math:`\lambda = 4\xi_0` (via :bash:`-k`), exposed to an external flux of :math:`\Phi_{\mathrm{ext}} = 12\Phi_0` (via :bash:`-B`):

.. code-block:: bash
   :caption: **Adding multiple vortices via command line**
   :name: example-cli-vortices

   python superconga.py simulate -C examples/dwave_plus_swave/ -k 4 -B 12 --vortex 5 5 -1 --vortex 5 -5 -1 --vortex -5 -5 -1 --vortex -5 5 -1 -S data/dwave_plus_swave_vortices

Once converged, the results are plotted via:

.. code-block:: bash
   :caption: **Plotting the d+is vortex example**
   :name: example-cli-vortices-plot

   python superconga.py plot-simulation -L data/dwave_plus_swave_vortices

which should generate a plot similar to:

.. figure:: _static/images/tutorials/dwave_plus_swave_plot_data.png
    
    Figure: Four vortices in a :math:`d+is` superconductor. Open image in new tab for larger version.

6.5. Example: spontaneous symmetry breaking in d+ig
---------------------------------------------------
In this example, we study how spontaneous currents and fluxes can arise in a :math:`d+ig` superconductor, due to "phase crystallization". See the `guides`_ and the `related publications`_ for more information on this phenomenon.

We use the existing example :bash:`examples/dwave_plus_swave`, but replace the :math:`s`-wave component with a :math:`g`-wave:

.. code-block:: bash
   :caption: **Modifying the dwave_plus_swave example to study a d+ig superconductor**
   :name: example-order-parameter-subdominant-example

   python superconga.py simulate -C examples/dwave_plus_swave/simulation_config.json --no-s-wave --g-wave 0.5 0.01 0 -T 0.1 -k 80 -S data/dwave_plus_gwave

Here, the relative transition temperature of the *g*-wave component is set to half that of the *d*-wave component. Furthermore, the temperature is changed to :math:`T=0.1T_{\mathrm{c}}`, the penetration depth to :math:`80\xi_0`, and the results data folder is changed to :bash:`data/dwave_plus_gwave`. The low temperature leads to a huge energy cost of the zero-energy Andreev-bound states (surface flat bands) close to the :math:`[110]`-interfaces (i.e. all parts of the grain that are roughly parallel to the xy-axes). This leads to that time-reversal symmetry breaking can be energetically favorable, since the associated spontaneous superflow can shift the states to finite energies, which will lower the energy. To find this symmetry-broken phase, the phase shift of :math:`0.01 \times 2\pi` in the above example is necessary (i.e. :bash:`--g-wave 0.5 0.01 0`), since the system will otherwise find the meta-stable state without spontaneous currents. We verify this by running the simulation with and without the phase shift, and compare the total free energy found under the field :bash:`"free_energy_per_unit_area"` in the file :bash:`data/dwave_plus_gwave/simulation_results.json`. The spontaneous currents lower the free energy from :math:`\Omega \approx -0.017 \Omega_{\mathcal{A}}` to :math:`\Omega \approx -0.018 \Omega_{\mathcal{A}}`, where :math:`\Omega_{\mathcal{A}} \equiv (2\pi k_{\mathrm{B}}T_{\mathrm{c}})^2 N_{\mathrm{F}}\mathcal{A}` with grain area :math:`\mathcal{A}`.

To visualize these spontaneous currents, we run the SuperConga plotter on the converged data:

.. code-block:: bash
   :caption: **Plotting the results of the d+ig simulation**
   :name: example-order-parameter-subdominant-example-plot

   python superconga.py plot-simulation -L data/dwave_plus_gwave --view {D,J}

which should generate a plot similar to the following figure:

.. figure:: _static/images/tutorials/dwave_plus_gwave.png
    
    Figure: Spontaneous currents in a d+ig superconductor.

The DOS and LDOS can be calculated via:

.. code-block:: bash
   :caption: **Computing the DOS and LDOS for the d+ig simulation**
   :name: example-order-parameter-subdominant-example-postprocess

   python superconga.py postprocess -C examples/dwave_plus_swave/ -L data/dwave_plus_gwave/ -S data/dwave_plus_gwave/

Here, we use the DOS config :bash:`examples/dwave_plus_swave/postprocess_config.json` from the original example (which is OK since it does not contain specific settings for the order-parameter). We also specify the load path of the order-parameter data and system config (via :bash:`-L`), as well as the save path of the DOS data (via :bash:`-S`). Upon finishing the postprocessing, the DOS and LDOS can be inspected via

.. code-block:: bash
   :caption: **Plotting the DOS and LDOS for the d+ig simulation**
   :name: example-order-parameter-subdominant-example-postprocess-plot

   python superconga.py plot-postprocess -L data/dwave_plus_gwave

which should generate a plot similar to:

.. figure:: _static/images/tutorials/dwave_plus_gwave_dos.png
    
    Figure: Doppler-shifted zero-energy states in d+ig superconductor, due to spontaneous currents.

Without the spontaneous currents, there would be a huge zero-energy peak in the point where the LDOS is plotted (red marker in left panel, red curve in right panel). Instead, we see a significant broadening, and two peaks at finite energies.

.. _`guides`: guides/phase-crystals.html
.. _`related publications`: about.html#research-using-superconga


6.6. Example: chiral d-wave superconductor
------------------------------------------
This example shows how SuperConga can be used to study chiral currents and domain walls in a chiral :math:`d_{x^2-y^2} + id_{xy}` superconductor (or just :math:`d+id^{\prime}` for short).

Chiral currents
^^^^^^^^^^^^^^^
The example :bash:`examples/dwave_chiral/` simulates a chiral *d*-wave superconductor. To find the chiral phase, an initial phase shift must typically be applied to one of the components. This is demonstrated by the field :bash:`"initial_phase_shift": 0.25,` of the second component in :bash:`examples/dwave_chiral/simulation_config.json`:

.. code-block:: bash
   :caption: **Order-parameter part of the chiral d-wave example**
   :name: example-dwave-chiral-dwave-op-sym

    "order_parameter": {
        "dx2-y2": {
            "critical_temperature": 1.0,
            "initial_phase_shift": 0.0,
            "initial_noise_stddev": 0.0,
            "vortices": []
        },
        "dxy": {
            "critical_temperature": 1.0,
            "initial_phase_shift": 0.25,
            "initial_noise_stddev": 0.0,
            "vortices": []
        }
    }

Running the example is done via:

.. code-block:: bash
   :caption: **Running the chiral d-wave example**
   :name: example-chiral-dwave-run

   python superconga.py simulate -C examples/dwave_chiral/

The data can then be plotted:

.. code-block:: bash
   :caption: **Plotting the chiral d-wave example**
   :name: example-chiral-dwave-plot

   python superconga.py plot-simulation -L data/examples/dwave_chiral/ --view {D,J}

which will generate a figure similar to below:

.. figure:: _static/images/tutorials/dwave_chiral_plot_data.png
    
    Figure: Plot from the chiral *d*-wave example in :bash:`examples/dwave_chiral/`.

Here we note in particular the chiral currents running along the edges of the system, caused by a finite and quantized angular momentum :math:`l_z` of the condensate. The latter is directly connected with the phase shift between components. Therefore, changing the phase shift to :bash:`-0.25` will instead lead to the degenerate state :math:`d-id^{\prime}` state, and a reversal of both the angular momentum and chiral currents. We show this explicitly below. Furthermore, we note a sign-change in the chiral current a distance from the surface, which is caused by the chiral *d*-wave having two counter-propagating chiral edge modes. More generally, the number of edge modes is determined by the order of chirality (i.e. 1, 2, 3 for chiral *p*-wave, *d*-wave, *f*-wave, respectively).

Chiral transformation
^^^^^^^^^^^^^^^^^^^^^
To explicitly see the chirality, the flag :bash:`--chiral-transform` can be used with the :bash:`plot-simulation` subcommand. It transforms the order parameter according to: :math:`|\Delta_{\pm}|e^{i\chi_\pm} = \left(|\Delta_{d_{x^2-y^2}}|e^{i\chi_{d_{x^2-y^2}}} \mp i|\Delta_{d_{xy}}|e^{i\chi_{d_{xy}}}\right)/\sqrt{2}`. Note the perhaps counter-intuitive sign in the left-hand side. The notation comes instead from the angular momentum definition: these order parameters are expressed in the eigenbasis of the angular momentum operator :math:`\hat{L}_z` via :math:`\Delta_{\pm} = |\Delta_{\pm}|e^{i\chi_{\pm}}\eta_{\pm}` with :math:`\eta_{\pm} = e^{\pm 2i\theta_{\mathrm{F}}}`. They correspond to the two degenerate chiralities with eigenvalues :math:`l_z = \pm 2`, respectively. 

We plot the data from the chiral *d*-wave example using this transformation:

.. code-block:: bash
   :caption: **Plotting the chiral d-wave example (chiral transform)**
   :name: example-chiral-dwave-plot-transformed

   python superconga.py plot-simulation --chiral-transform -L data/examples/dwave_chiral/ --view {D,J}

which will generate a figure similar to below:

.. figure:: _static/images/tutorials/dwave_chiral_plot_data_transformed.png
    
    Figure: Plot of a :math:`d+id^{\prime}` superconductor.

If we instead re-run the simulation with the opposite chirality and plot it:

.. code-block:: bash
   :caption: **Simulating and plotting the modified chiral d-wave example (chiral transform)**
   :name: example-chiral-dwave-plot-transformed-opposite

   python superconga.py simulate -C examples/dwave_chiral/ --dxy-wave 1 -0.25 0 -S data/examples/dwave_chiral_opposite/
   python superconga.py plot-simulation --chiral-transform -L data/examples/dwave_chiral_opposite/ --view {D,J}

it will instead generate this figure:

.. figure:: _static/images/tutorials/dwave_chiral_plot_data_transformed_opposite.png
    
    Figure: Plot of a :math:`d-id^{\prime}` superconductor.


Chiral domain walls
^^^^^^^^^^^^^^^^^^^
In the following example, we will see how domain walls form in a thin slab geometry. Note that for more information about how to set up general geometries, see `Lesson 7`_.

A slab geometry can be created via command-line, by adding several squares next to each other (please note that the simulation might be difficult to converge):

.. code-block:: bash
   :caption: **Simulating a chiral d-wave in a slab geometry**
   :name: example-chiral-dwave-slab-run

   python superconga.py simulate -C examples/dwave_chiral/ --dxy-wave 1 0.25 20 --dxy-wave 1 0.25 20 -T 0.1 -S data/dwave_chiral_slab/ --clear-geometry --add-regular-polygon 0 0 4 0 10 --add-regular-polygon 10 0 4 0 10 --add-regular-polygon 20 0 4 0 10 --add-regular-polygon 30 0 4 0 10 --add-regular-polygon 40 0 4 0 10

Here, we first add significant noise to both components to seed the domain walls, then change the temperature to :math:`T=0.1T_{\mathrm{c}}`, and then use :bash:`--clear-geometry` to remove whatever geometry was added from the original config file. The command :bash:`--add-regular-polygon` is used several times to add regular polygons with 4 edges (squares). The numbers following this argument are (in order): :bash:`center_x center_y num_edges rotation side_length`. The coordinates and lengths are given in units of :math:`\xi_0`, and the rotation in units of a full rotation (:math:`2\pi`). See the command-line help for more information. Alternatively, the slab geometry can also be entered in the config file, as a rectangle via the free-form polygon key:

.. code-block:: bash
   :caption: **Slab geometry**
   :name: example-dwave-chiral-dwave-op-sym-thin-slab

   "geometry": [
        {
            "polygon": {
                "add": true,
                "vertices_x": [
                    50.0,
                    0.0,
                    0.0,
                    50.0
                ],
                "vertices_y": [
                    5.0,
                    5.0,
                    -5.0,
                    -5.0
                ]
            }
        }
    ],

The data from the simulation can be plotted via:

.. code-block:: bash
   :caption: **Plotting data of a chiral d-wave in a slab geometry**
   :name: example-chiral-dwave-slab-plot

   python superconga.py plot-simulation -L data/dwave_chiral_slab/ --view {D,J}

which will generate something similar to the following plot (variations might occur due to the random phase in the start guess):

.. figure:: _static/images/tutorials/chiral_dwave_thin_slab.png
    
    Figure: Domain walls in a thin chiral *d*-wave superconductor.

Here, two vertical domain walls form in the middle of the slab, as indicated by the local reduction in the :math:`d_{xy}` magnitude. We can also visualize the result in terms of :math:`\Delta_{\pm}`, using the :bash:`--chiral-transform` flag as in the previous example:

.. code-block:: bash
   :caption: **Plotting data of a chiral d-wave in a slab geometry (chiral transform)**
   :name: example-chiral-dwave-slab-plot-transformed

   python superconga.py plot-simulation --chiral-transform -L data/dwave_chiral_slab/ --view {D,J}

which yields the following figure:

.. figure:: _static/images/tutorials/chiral_dwave_thin_slab_transformed.png
    
    Figure: Domain walls in a thin chiral *d*-wave superconductor (chiral transformed).

.. _`Lesson 7`: tutorials.html#lesson-7-geometry-setup

----

Lesson 7: Geometry setup
========================
This tutorial shows how to set up arbitrary 2D geometries, using a combination of discs and polygons. It will be shown that by adding and removing them in succession, advanced compound geometries can be created. Finally, we will use the vortex lattice example as a starting point, to show how these geometries give rise to mesoscopic vortex effects.

.. admonition:: Lesson 7

  - **Files:** :bash:`examples/swave_abrikosov_lattice/simulation_config.json`, :bash:`examples/swave_disc_vortex/simulation_config.json`, :bash:`examples/dwave_octagon/simulation_config.json`, and :bash:`examples/dwave_phase_crystal/simulation_config.json` 
  - **Learning Objectives:**

     - disc geometries
     - regular polygons
     - arbitrary polygons
     - advanced compound geometries

7.1. The geometry section of the configuration file
---------------------------------------------------
The geometry can either be modified via the :bash:`.json` config-files, or via command line. For the latter, see in particular the geometry section under the help: :bash:`python superconga.py simulate --help`.

The following shows the geometry section of :bash:`examples/swave_abrikosov_lattice/simulation_config.json`:

.. code-block:: bash
   :caption: **Geometry section of the config file**
   :name: example-geometry-section-of-config

   "geometry": [
        {
            "regular_polygon": {
                "add": true,
                ...
            }
        }
    ],

The geometry section starts with braces :bash:`[ ... ]`, indicating that the geometry is a list of different geometry components. In this case, the geometry consists of a single component of type :bash:`"regular_polygon"`. The valid types are:

- :bash:`"disc"` - a perfect disc. For quasiparticle trajectories, the boundary is perfectly round. Example: :bash:`examples/swave_disc_vortex/simulation_config.json`.
- :bash:`"regular_polygon"` - a regular *n*-gon, e.g. a square, hexagon, or pentagon. Example: :bash:`examples/dwave_octagon/simulation_config.json`.
- :bash:`"polygon"` - an arbitrary polygon, e.g. a rectangle or irregular triangle. Example: :bash:`examples/dwave_phase_crystal/simulation_config.json`.

The valid settings vary for each of these types, but all of them must have the field :bash:`"add"`, which is boolean. A value of :bash:`true` or :bash:`false` indicates that the component is either added or subtracted from the total geometry, making it possible to create more advanced connected geometries and holes. The following tutorials go through each geometry type in detail, followed by a section about compound geometries (i.e. a geometry with multiple components).

7.2. Discs
----------
The following shows the geometry section of :bash:`examples/swave_disc_vortex/simulation_config.json`, which creates a superconductor shaped like a disc:

.. code-block:: bash
   :caption: **Disc geometry**
   :name: example-disc-geometry

    "geometry": [
        {
            "disc": {
                "add": true,
                "center_x": 0.0,
                "center_y": 0.0,
                "radius": 15.0
            }
        }
    ],

Here, the fields :bash:`"radius"`, :bash:`"center_x"` and :bash:`"center_y"` are given in coherence lengths (:math:`\xi_0`). The latter two gives the center-coordinate of the disc. This is only relevant when having multiple components in the geometry, and the user can choose their own origin/coordinate system. This is because the framework internally translates and rescales the geometry. The observables and quantities represented inside the disc have a discrete representation, which means that the disc geometry will appear with finite pixels and aliasing around the perimeter. In terms of boundary conditions and trajectories though, the exact boundary is represented as perfectly round (i.e. the exact boundary location and normal are stored).

To create the above disc via command line instead, we can run the following:

.. code-block:: bash
   :caption: **Create a disc geometry via command line**
   :name: example-create-disc

   python superconga.py simulate -C examples/dwave_octagon -S data/dwave_disc --clear-geometry --add-disc 0 0 15

Here, we load the config from the *d*-wave octagon, but use the command :bash:`--clear-geometry` to remove the octagon, and then use :bash:`--add-disc 0 0 15` to create a disc with radius :math:`15\xi_0` at :math:`(x,y) = (0,0)\xi_0`. Similarly, a disc can be removed from the geometry via :bash:`--remove-disc`, to create e.g. holes. The results can be plotted via:

.. code-block:: bash
   :caption: **Plot d-wave disc**
   :name: example-create-disc-plot

   python superconga.py plot-simulation -L data/dwave_disc --view {D,J}

which should generate the following image:

.. figure:: _static/images/tutorials/dwave_disc.png

    Figure: *d*-wave superconductor in disc geometry. Parts of the disc which are approximately [110] interfaces leads to resonant Andreev-reflection, suppressing the order parameter and inducing zero-energy flat bands.

.. note::
    The coordinates and lengths used to specify the geometry are given in coherence lengths. The choice of origin/coordinate system is arbitrary and up to the user to choose as they please.

7.3. Regular polygons
---------------------
The following shows the geometry section of :bash:`examples/dwave_octagon/simulation_config.json`, which creates a superconductor shaped like an octagon:

.. code-block:: bash
   :caption: **Regular polygon geometry: an octagon**
   :name: example-regular-polygon-geometry

    "geometry": [
        {
            "regular_polygon": {
                "add": true,
                "center_x": 0.0,
                "center_y": 0.0,
                "num_edges": 8,
                "rotation": 0.0,
                "side_length": 10.0
            }
        }
    ],

Here, :bash:`"side_length"` is the side length of the regular polygon in coherence lengths, as are the center coordinates of the polygon. The field :bash:`"num_edges"` determines the number of edges of the polygon, in this case 8 for an octagon. Finally, the :bash:`"rotation"` specifies the rotation of the polygon, in units of a full rotation (:math:`2\pi`).

To create a hexagon via command line, we can run the following:

.. code-block:: bash
   :caption: **Create a hexagon geometry via command line**
   :name: example-create-hexagon

   python superconga.py simulate -C examples/dwave_octagon -S data/dwave_hexagon --clear-geometry --add-regular-polygon 0 0 6 0.333 20

Like in the previous tutorial, we first clear the geometry, and then use :bash:`--add-regular-polygon 0 0 6 0.333 20` to create an *n*-gon with 6 vertices (hexagon), with side-length :math:`20\xi_0` at :math:`(x,y) = (0,0)\xi_0`, rotated with :math:`0.333 \times 2\pi`. Similarly, a regular polygon can be removed from the geometry via :bash:`--remove-regular-polygon`, to create e.g. wedges and holes. The results can be plotted via:

.. code-block:: bash
   :caption: **Plot d-wave hexagon**
   :name: example-create-hexagon-plot

   python superconga.py plot-simulation -L data/dwave_hexagon --view {D,J}

which should generate the following image:

.. figure:: _static/images/tutorials/dwave_hexagon.png

    Figure: Regular polygon geometry, showing a *d*-wave hexagon with two [100] interfaces (left and right), and four approximately [110] interfaces (top and bottom). The order parameter is suppressed at the [110] interfaces due to resonant Andreev reflection.

7.4. Arbitrary polygons
-----------------------
The following shows the geometry section of :bash:`examples/dwave_phase_crystal/simulation_config.json`, which creates a superconductor shaped like an irregular polygon, namely a square with a corner cut away (see image below):

.. code-block:: bash
   :caption: **Arbitrary polygon geometry: d-wave phase crystal example**
   :name: example-polygon-geometry

    "geometry": [
        {
            "polygon": {
                "add": true,
                "vertices_x": [
                    25.0,
                    0.0,
                    0.0,
                    5.0,
                    25.0
                ],
                "vertices_y": [
                    25.0,
                    25.0,
                    0.0,
                    0.0,
                    20.0
                ]
            }
        }
    ],

Here, the fields :bash:`"vertices_x"` and :bash:`"vertices_y"` are lists of the vertex (corner) coordinates of the polygon, in units of coherence lengths. The lists must have the same length (since the x- and y- coordinates are paired). Please note that the coordinates must be given in counter-clockwise order. The arbitrary polygon geometry in the example above is shown in the following image:

.. figure:: _static/images/tutorials/arbitrary_polygon_phase_crystal.png

    Figure: Arbitrary polygon geometry, in this case showing a *d*-wave superconductor with a single [110] interface, hosting spontaneous surface currents. This is a realization of a surface "|phase crystal|". Open image in new tab for larger version.

.. |phase crystal| raw:: html

    <a href="https://journals.aps.org/prresearch/abstract/10.1103/PhysRevResearch.2.013104" target="_blank">phase crystal</a>

Please note that the arbitrary polygons currently cannot be created from command line, and only support a "convex hull", which means that exclusions/indentations/holes and similar cannot be created with a single polygon. For this, an additional polygon (or disc) can be removed from the geometry, using the setting :bash:`"add": false`. The following part of the tutorial shows how this can be done, to create advanced geometries.

.. note::

    Arbitrary polygons are defined by specifying the corner coordinates in counter-clockwise order.

    Arbitrary polygons only support "convex hull".

    Arbitrary polygons currently cannot be created from command line.

The following tutorials show how to combine the previous geometry components to create a compound geometry. In particular, it is shown how to make rounded corners, holes, and other features.

7.5. Compound geometries: holes
-------------------------------
A hole can be created in the superconductin grain via the argument :bash:`"add": false` in the config file, or by command line using either :bash:`--remove-disc ...` or :bash:`--remove-regular-polygon ...` (the latter two must be followed by additional arguments specifying the disc/polygon, see the command-line help). The following shows the geometry-part of a config file with a hole:

.. code-block:: bash
   :caption: **Geometry with a circular hole**
   :name: example-disc-remove-config

	"geometry": [
		{
			"regular_polygon": {
				"add": true,
				"center_x": 0.0,
				"center_y": 0.0,
				"num_edges": 4,
				"rotation": 0.0,
				"side_length": 20.0
			}
			"disc": {
				"add": false,
				"center_x": 0.0,
				"center_y": 0.0,
				"radius": 5.0
			}
		}
	],

In the above config file, a square is added, then a disc is removed from the square.

.. note::
    When adding and removing objects, the order matters.

7.6. Compound geometries: solenoids
-----------------------------------
Below, we remove an octagon from an existing example, and apply a solenoid magnetic field. The latter corresponds to a magnetic flux that is non-zero in the excluded region (the hole), and zero in the superconductor. This can be created experimentally by letting the superconductor encircle a ferromagnet (of radius :math:`R_{<}`), exactly like in a solenoid. This creates a magnetic vector pontential with radial dependence :math:`\mathbf{A}_{\mathrm{ext}}(\mathbf{R}) = \frac{\Phi_{\mathrm{ext}}}{2\pi R}\hat{\mathbf{\varphi}}` inside the superconducting region (:math:`R_{<} < R`). Here, :math:`\Phi_{\mathrm{ext}}` is the external flux, and :math:`\varphi` is the polar angle. See the SuperConga v1.0 manuscript for more information about this field, including a comparison between analytics and numerics.

The solenoid field can be enabled in SuperConga by either setting :bash:`"gauge": "solenoid"` in the config file, or by using command-line argument :bash:`-g|--gauge solenoid`. Other valid gagues are :bash:`landau` and :bash:`symmetric`. The following example applies the solenoid gauge in a :math:`d+is` superconductor, with :math:`\Phi_{\mathrm{ext}} = 3.7\Phi_0`:

.. code-block:: bash
   :caption: **Simulating and plotting a solenoid setup**
   :name: example-otagon-remove

   python superconga.py simulate -C examples/dwave_plus_swave -S data/dwave_plus_swave_modified -g solenoid --remove-regular-polygon 0 0 8 0.333 5 -B 3.7 -N 10 -p 64 -c 2.5e-5

   python superconga.py plot-simulation -L data/dwave_plus_swave_modified --view {D,J}

.. figure:: _static/images/tutorials/dwave_plus_swave_modified.png

    Figure: Octagon removed from a disc, in a :math:`d+is` superconductor. The *s*-wave is mainly induced along the nodes of the *d*-wave. A "solenoid magnetic field" is applied through the hole.

Please note that this example can be difficult to converge, and might need higher numerical discretization/accuracy due to the pathological system.

7.7. Compound geometries: rounded corners
-----------------------------------------
By adding/removing discs from a geometry, rounded corners can be created. The following example shows a rectangle where the corners have been rounded:

.. code-block:: bash
   :caption: **Simulating and plotting a rectangle with rounded corners**
   :name: example-rounded-corners

    "geometry": [
        {
            "disc": {
                "add": true,
                "center_x": 0.0,
                "center_y": 0.0,
                "radius": 5.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 0.0,
                "center_y": 20.0,
                "radius": 5.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 40.0,
                "center_y": 0.0,
                "radius": 5.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 40.0,
                "center_y": 20.0,
                "radius": 5.0
            }
        },
        {
            "polygon": {
                "add": true,
                "vertices_x": [0, 40, 45, 45, 40, 0, -5, -5],
                "vertices_y": [-5, -5, 0, 20, 25, 25, 20, 0]
            }
        }
    ],

.. figure:: _static/images/tutorials/dwave_chiral_rounded_square.png

    Figure: Chiral *d*-wave superconductor in a rectangle with rounded corners.

7.8. Compound geometries: connecting shapes
-------------------------------------------
In this example, we add multiple geometries to essentially create several superconducting grains connected by narrow channels. The geometry-part of the config file used is:

.. code-block:: bash
   :caption: **Geometry part of config: connecting grains with thin channels**
   :name: example-connecting-grains
   
    "geometry": [
        {
            "disc": {
                "add": true,
                "center_x": -8.0,
                "center_y": 0.0,
                "radius": 20.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 68.0,
                "center_y": 0.0,
                "radius": 20.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 30.0,
                "center_y": 50.0,
                "radius": 20.0
            }
        },
        {
            "polygon": {
                "add": true,
                "vertices_x": [
                    10,
                    10,
                    50,
                    50
                ],
                "vertices_y": [
                    4,
                    -4,
                    -4,
                    4
                ]
            }
        },
        {
            "polygon": {
                "add": true,
                "vertices_x": [
                    26,
                    34,
                    34,
                    26
                ],
                "vertices_y": [
                    0,
                    0,
                    50,
                    50
                ]
            }
        }
    ],
   
Running the example :bash:`examples/dwave_chiral` with this geometry will yield the following plot:

.. figure:: _static/images/tutorials/dwave_chiral_connected_grains.png

    Figure: Chiral *d*-wave superconductor two superconducting grains connected via a narrow channel.

.. warning::
    Quasiclassics can break down in narrow geometries. Therefore, it is recommended to avoid narrow channels, like parallel interfaces that are separated by only a few :math:`\xi_0`.

7.9. Compound geometries: disconnected grains and narrow exclusions
-------------------------------------------------------------------
There is nothing that a priori says that the superconducting grains have to be connected. The following example shows how to create superconducting grains that are completely disconnected, or connected only via magnetic induction. The geometry part of the config file used is:

.. code-block:: bash
   :caption: **Geometry part of config: disconnected grains**
   :name: example-disconnected-grains

    "geometry": [
        {
            "disc": {
                "add": true,
                "center_x": 0.0,
                "center_y": 0.0,
                "radius": 20.0
            }
        },
        {
            "disc": {
                "add": false,
                "center_x": 0.0,
                "center_y": 0.0,
                "radius": 13.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 0.0,
                "center_y": 0.0,
                "radius": 10.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 40.0,
                "center_y": -20.0,
                "radius": 10.0
            }
        },
        {
            "disc": {
                "add": true,
                "center_x": 40.0,
                "center_y": 20.0,
                "radius": 10.0
            }
        }
    ],

Running the example :bash:`examples/dwave_chiral` with this geometry and :math:`\kappa = 15` will yield the following plot:

.. figure:: _static/images/tutorials/dwave_chiral_disconnected_grains.png

    Figure: Multiple disconnected superconducting grains. They are still interacting weakly via magnetic induction. Open image in new tab for larger version.

7.10. Compound geometries: some final examples
----------------------------------------------
Now that we have shown most concepts, it is straightforward to create even more advanced and less symmetric geometries. In this final part, we highlight some examples that can be created with SuperConga. The configurations either follow trivially from the above tutorials, or are too lengthy to show here. Instead, we let them serve as an inspiration.

By removing arbitrary polygons, it is possible to create mesoscopic roughness, similar to the following example:

.. figure:: _static/images/tutorials/mesoscopic_roughness.png

    Figure: Spontaneous flux in grains with mesoscopic roughness, adapted from |prb_2019|.

.. |prb_2019| raw:: html

    <a href="https://journals.aps.org/prb/abstract/10.1103/PhysRevB.99.184511" target="_blank">PRB 99, 184511 (2019)</a>

The following shows an example of disconnected grains, shaped like letters:

.. figure:: _static/images/tutorials/mc2_shape.png

    Figure: Spontaneous currents in disconnected grains shaped like the Chalmers MC2 logotype. Adapted from |holmvall_lic|.

.. |holmvall_lic| raw:: html

    <a href="https://research.chalmers.se/en/publication/253315" target="_blank">Modeling mesoscopic unconventional superconductors</a>

The following figure shows a collage of some example geometries created in SuperConga, that were inspired by real experimental setups:

.. figure:: _static/images/tutorials/experimental_grains_simulated.png

    Figure: Grain geometries inspired by real experiments, e.g. dayem bridges and SQUID-like setups.

----

Lesson 8: Parameter sweeps
==========================
This tutorial shows how to vary parameters in a range, which is useful to get for example the temperature-dependence of a phenomenon, and to extract phase diagrams. Although the backend in principle supports variations of parameters during a simulation, the framework is currently designed to encourage the variation externally instead, meaning that a unique simulation should be run for each unique value of a parameter. One reason for this is that SuperConga currently only deals with static equilibrium: only the final converged solution makes sense pysically, meaning that variations during a simulation are not related to dynamics but rather to "iteration-dynamics". Still, it might be useful to do "annealing" of temperature and flux to go between different configurations in phase space, and this is certainly possible by turning on/off burn-in when resuming simulations, but this should never be confused with real dyanmics.

The tutorial shows how to set up a series of simulations to sweep parameters like temperature, external field strength and penetration depth. To aid with this, we use bash scripts.

.. admonition:: Lesson 8

  - **Files:** :bash:`examples/parameter_sweeps/swave_disc_vortex_kappa_sweep.sh`, :bash:`examples/swave_disc_vortex/simulation_config.json`, :bash:`examples/swave_disc_meissner/simulation_config.json`
  - **Learning Objectives:**

     - running simulations via shell scripts
     - series of simulations with varying parameters, e.g. temperature, field

8.1. Sweeping penetration depth: existing examples
--------------------------------------------------
Below, we see the contents of the file :bash:`examples/parameter_sweeps/swave_disc_vortex_kappa_sweep.sh`:

.. code-block:: bash
   :caption: **swave_disc_vortex_kappa_sweep.sh**
   :name: example-swave-vortex-sweep

    #!/bin/bash

    # Note, this script is meant to be executed from the root folder of the project.
    # ./examples/parameter_sweeps/swave_disc_vortex_kappa_sweep.sh

    # The configuration file to use.
    CONFIG="examples/parameter_sweeps/kappa_sweep_config.json"

    # Base path to save the data to.
    DATA_PATH="data/examples/swave_disc_vortex_kappa_sweep"

    # The number of burn-in iterations.
    BURNIN=-1

    python superconga.py simulate -C $CONFIG -S $DATA_PATH/kInf --no-visualize -k -1 --vortex 0 0 -1
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k64 -L $DATA_PATH/kInf -b $BURNIN --no-visualize -k 64
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k32 -L $DATA_PATH/k64 -b $BURNIN --no-visualize -k 32
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k16 -L $DATA_PATH/k32 -b $BURNIN --no-visualize -k 16
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k8 -L $DATA_PATH/k16 -b $BURNIN --no-visualize -k 8
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k4 -L $DATA_PATH/k8 -b $BURNIN --no-visualize -k 4 
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k2 -L $DATA_PATH/k4 -b $BURNIN --no-visualize -k 2
    python superconga.py simulate -C $CONFIG -S $DATA_PATH/k1 -L $DATA_PATH/k2 -b $BURNIN --no-visualize -k 1

As can be seen, it manually calls SuperConga several times, but with unique values of the penetration depth each time. Each simulation uses the data from the previous one as a starting point, to speed up convergence. The script can be called from the SuperConga root directory via:

.. code-block:: bash
   :caption: **Running the script swave_disc_vortex_kappa_sweep.sh**
   :name: example-swave-vortex-sweep-run

    ./examples/parameter_sweeps/swave_disc_vortex_kappa_sweep.sh

In the next example, we show how this concept can be taken further, by looping over two parameters to e.g. extract the Meissner phase diagram.

8.2. Sweeping more parameters: extracting Meissner-phase diagram
----------------------------------------------------------------
In this example, we show how to sweep the external flux and temperature for both the vortex and Meissner examples. The resulting free energies can be compared to extract the temperature-dependence of first critical field, :math:`B_{\mathrm{c}1}(T)`, at which it becomes favorable for an Abrikosov vortex to enter the system. In other words, such data would yield the entire phase diagram of the Meissner phase. The temperature-dependence of the free energy can even be used to calculate the entropy and heat capacity, to characterize phase transitions.

Below, we show the example bash script:

.. code-block:: bash
   :caption: **flux_temperature_sweep.sh**
   :name: example-flux-temperature-sweep

    #!/bin/bash
    # flux_temperature_sweep.sh - sweep flux and temperature, for vortex and Meissner.

    # Path to base configuration files.
    VORTEX_CONFIG="examples/swave_disc_vortex/simultation_config.json"
    MEISSNER_CONFIG="examples/swave_disc_meissner/simultation_config.json"

    # Base path of data output.
    VORTEX_BASE_PATH="data/flux_temperature_sweep/vortex"
    MEISSNER_BASE_PATH="data/flux_temperature_sweep/meissner"

    # Loop over temperatures in steps of 0.1 Tc.
    T_MIN=0.1
    T_MAX=0.9
    T_STEP=0.1
    T_IDX=0
    for T in $(seq $T_MIN $T_STEP $T_MAX); do
        # Loop over flux in steps of 0.5 flux quanta.
        FLUX_MIN=0
        FLUX_MAX=6
        FLUX_STEP=0.5
        for FLUX in $(seq $FLUX_MIN $FLUX_STEP $FLUX_MAX); do
            # Create unique data path for each simulation.
            VORTEX_SAVE=`printf "$VORTEX_BASE_PATH/T_%2.1f/phi_%2.1f" $T $FLUX`
            MEISSNER_SAVE=`printf "$MEISSNER_BASE_PATH/T_%2.1f/phi_%2.1f" $T $FLUX`

            # Use previous temperature, when possible.
            if [[ "$T_IDX" -gt 0 ]]; then
                VORTEX_LOAD=`printf "-L $VORTEX_BASE_PATH/T_%2.1f/phi_%2.1f" $T_PREVIOUS $FLUX`
                MEISSNER_LOAD=`printf "-L $MEISSNER_BASE_PATH/T_%2.1f/phi_%2.1f" $T_PREVIOUS $FLUX`
            else
                VORTEX_LOAD=""
                MEISSNER_LOAD=""
            fi

            # Run simulations.
            python superconga.py simulate -C $VORTEX_CONFIG -S $VORTEX_SAVE --no-visualize -B $FLUX -T $T $VORTEX_LOAD
            python superconga.py simulate -C $MEISSNER_CONFIG -S $MEISSNER_SAVE --no-visualize -B $FLUX -T $T $MEISSNER_LOAD
        done
        # Store current temperature for next iteration.
        T_PREVIOUS=$T
        T_IDX=$((IDX+1))
    done

The above can naturally be refined, or made into e.g. a Python script, but it shows the main principle of parameter sweeps, extracting parameter-dependence, and even phase diagrams.

----

Lesson 9: Adding noise
======================
In this tutorial, we show how Gaussian noise can be added to the order parameter and the vector potential, via the Python tool :bash:`tools/noise.py` in the repository. This is useful for perturbing a solution, for example testing (meta-)stability.

As concrete examples, we will study configurations with giant vortices. Quite generally, these objects are unstable in completely homogeneous and clean environments. Unless such effects are introduced, these objects tend to decay in Ginzburg-Landau theory and Quasiclassics. It can be difficult to show this numerically in quasiclassics though, especially for certain values of the temperatures and penetration depth. This is where the noise tool comes in: we show that adding a very small perturbation to the order parameter is sufficient to get the unstable solutions to decay. We note that in fully microscopic theories, quantum size effects might be enough to make giant vortices metastable.

9.1. The noise tool
-------------------
The noise tool is located in :bash:`tools/noise.py`, and its help message can be viewed by running it from the SuperConga root directory:

.. code-block:: bash
   :caption: **SuperConga noise tool**
   :name: example-noise tool

   python tools/noise.py --help

To run it, the user has to specify the location of the existing data via :bash:`-L|--load-path`, and the where to save the data after adding the noise via :bash:`-S|--save-path`. Both of these are given via a relative path. Additionally, the user has to specify how much noise to add to the order parameter (via :bash:`--op-noise`) and/or vector potential (via :bash:`--vp-noise`), in terms of the standard deviation of the Gaussian noise, in units of :math:`2\pi k_{\mathrm{B}}T_{\mathrm{c}}`.

9.2. Giant vortices
-------------------
Giant vortices are characterized by a multiply-quantized phase winding in a single winding center. In other words, a vortex core with multiple flux quanta. 

We start from the *s*-wave Meissner example, located in :bash:`examples/swave_disc_meissner/`, but put in a giant vortex with phase winding :math:`-2\times2\pi`, change the external flux to :math:`\Phi_{\mathrm{ext}} = 4\Phi_0`, penetration depth to :math:`\lambda = 4\xi_0`, the temperature to :math:`T=0.9T_{\mathrm{c}}`, and increase the real-space resolution and momentum-space resolution:

.. code-block:: bash
   :caption: **Simulating and plotting: giant vortex**
   :name: example-giant-vortex

   python superconga.py simulate -C examples/swave_disc_meissner -S data/giant_vortex --vortex 0 0 -2 -T 0.9 -B 4 -k 4 -N 10 -p 64

   python superconga.py plot-simulation -L data/giant_vortex

.. figure:: _static/images/tutorials/giant_vortex.png

    Figure: Giant vortex (unstable), with an integer phase winding of two.

The simulation appears to converge with a meta-stable giant vortex (the free energy is higher than that of two isolated vortices). However, the high symmetry of the solution hides the fact that this solution is unstable. We demonstrate this by using the noise tool to add a very small amount of Gaussian noise to the order parameter, with standard deviation :math:`0.01 \times 2\pi k_{\mathrm{B}}T_{\mathrm{c}}`:

.. code-block:: bash
   :caption: **Adding noise to giant vortex**
   :name: example-giant-vortex-noise

   python tools/noise.py -L data/giant_vortex -S data/giant_vortex_noise --op-noise 0.01

.. figure:: _static/images/tutorials/giant_vortex_noise.png

    Figure: Giant vortex with noise added.

We then resume the simulation, and plot the results:

.. code-block:: bash
   :caption: **Simulating and plotting the noise-perturbed giant vortex**
   :name: example-giant-vortex-noise-resume

   python superconga.py simulate -C data/giant_vortex -L data/giant_vortex_noise -S data/giant_vortex_decay

   python superconga.py plot-simulation -L data/giant_vortex_decay

.. figure:: _static/images/tutorials/giant_vortex_decay.png

    Figure: Two isolated Abrikosov vortices, from a decayed giant vortex.

Hence, the small noise ensured that the giant-vortex solution is not perfectly symmetric, which was enough to show that it is unstable and decays.

----

Lesson 10: More on extracting results: custom data analysis
===========================================================
This tutorial will dive deeper into how to extract results from simulations. Specifically, we will go through how to parse the data and import it using custom Python scripts, instead of relying on the existing ones. Potentially, we might also explain how to access the results using other languages and tools.

10.1. Python: extracting area-averaged quantities
-------------------------------------------------
In this tutorial, we will show how to fetch the area-averaged quantities. In particular, we will fetch the free energy from sevral simulations, obtaining :math:`\Omega(T)`, and then using it to calculate the entropy :math:`S(T)` and heat capacity :math:`C_{\mathcal{V}}(T)`.

10.2. Python: extracting spatially dependent quantities
-------------------------------------------------------
In this tutorial, we will show how to fetch and analyze spatially dependent quantities. In particular, we will plot line cuts.

----

Running Unit tests
==================
Our goal is to set up tests for the crucial functionality and methods in SuperConga using Doctest. The Doctest dependency is fetched automatically via CMake during the setup of SuperConga. There are essentially three ways to run the unit tests:

1. automatically during the compilation step using :bash:`ctest`,
2. manually using :bash:`ctest` in the build folder :bash:`build/<type>/tests/` (where :bash:`<type>` is either release or debug),
3. manually running the test binaries in :bash:`build/<type>/tests/`.

CTest is the test-driver program provided by CMake, which like CMake is language-agnostic, and allows to aggregate multiple tests into test suites. The first method is the recommended approach, and is done by running the following after :bash:`python superconga.py setup`:

.. code-block:: bash

    python superconga.py compile --type Release --test

Release can of course be exchanged for Debug. If you for some reason want to follow method 2, then you can run all tests successively via:

.. code-block:: bash

    ctest [-jN]

inside either the folder :bash:`build/<type>/` or :bash:`build/<type>/tests`. Here, the :bash:`-jN` option allows to run multiple tests in parallel over CPU threads. In order to see the list of tests which can be run use

.. code-block:: bash

    ctest -N

To run a specific test ignoring all others, use

.. code-block:: bash

    ctest -R test_name

For more information on CTest, see e.g. the man pages (:bash:`man ctest`) and help (:bash:`ctest --help`).

.. _`technical reference`: technical_reference.html
