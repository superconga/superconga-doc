====================
 Contact and support
====================

Contact
-------

| For questions regarding research, the overall project, collaborations and such, please contact the `project supervisors`_.
| For questions related to the SuperConga framework or the source code, please contact our `lead developers`_.

| Alternatively, you can chat with us directly on |Slack|.


.. _`lead developers`: contributors.html#current-developers

.. _`project supervisors`: contributors.html#project-supervisors

.. |Slack| raw:: html

   <a href="http://superconga.slack.com/" target="_blank">Slack</a>

Running into issues
-------------------
| Have you found a potential bug? Please make sure to first look at the `FAQ`_ page and the |issues on GitLab|, to see if your issue is already known and has a solution. If not, you can open a |Bug issue on our GitLab repository|.

| Do you have an idea for the framework that you want to share with us? Open an issue with the label "Discussion". See the `contribution guide`_ for more information on how to contribute to the project.

.. |Bug issue on our GitLab repository| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/issues/new?issuable_template=Bug" target="_blank">Bug issue on our GitLab repository</a>

.. |issues on GitLab| raw:: html

   <a href="https://gitlab.com/superconga/superconga/-/issues" target="_blank">issues on GitLab</a>




.. _`contribution guide`: contribute.html

.. _`FAQ`: faq.html
