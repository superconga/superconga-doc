==================
 Physics guides
==================
.. role:: bash(code)
   :language: bash

This page contains how-to guides, showing how to use SuperConga to study various superconducting phenomena, research scenarios and to reproduce published results. In contrast, the `tutorials`_ explain how individual features are used. Note that the installation guide can be found |here|.

The physics guides are based on an older version of SuperConga, but the goal is to rewrite them for v1.0 as soon as possible.


.. _`tutorials`: ../tutorials.html

.. |here| raw:: html

   <a href="https://gitlab.com/superconga/superconga#hardware-requirements" target="_blank">here</a>



.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:

   basic-thermodynamics

   meissner-screening

   abrikosov-vortex-core

   vortex-core-spectroscopy

   abs-doppler-shift

   phase-crystals


|start-h2| Basic thermodynamics |end-h2|

This guide shows how to compute the free energy, entropy and heat-capacity of the superconducting state in a mesoscopic sample, in the range :math:`T/T_{\mathrm{c}} \in (0, 1)`. The guide can be used as a starting point to studying less trivial phase diagrams.

`Click here to go to the thermodynamics guide`_, to produce the following figure:

   .. _`Click here to go to the thermodynamics guide`: basic-thermodynamics.html

.. figure:: /_static/images/guides/thermodynamics.png

    Figure: Thermodynamics of the superconducting phase. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

----


|start-h2| Meissner screening |end-h2|

This guide shows how to study the spatial dependence of screening currents and induced fields in the Meissner state, for various values of the superconducting penetration depth.

`Click here to go to the Meissner screening current guide`_, to produce the following figure:

   .. _`Click here to go to the Meissner screening current guide`: meissner-screening.html

.. figure:: /_static/images/guides/meissner_screening.png

    Figure: Meissner screening currents and fields. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

----


|start-h2| Abrikosov-vortex core |end-h2|

This guide shows how to study the spatial dependence of the order parameter, currents and fields as distance from an Abrikosov vortex, for various values of the superconducting penetration depth.

`Click here to go to the Abrikosov-vortex core guide`_, to produce the following figure:

   .. _`Click here to go to the Abrikosov-vortex core guide`: abrikosov-vortex-core.html

.. figure:: /_static/images/guides/abrikosov_vortex_core.png

    Figure: Spatial dependence of currents and fields in an Abrikosov vortex. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

----


|start-h2| Vortex-core spectroscopy |end-h2|

This guide shows how to do vortex spectroscopy in *s*-wave and *d*-wave superconductors, e.g. using the interactive density-of-states plotter.

`Click here to go to the interactive vortex spectroscopy guide`_, to produce the following figure:

   .. _`Click here to go to the interactive vortex spectroscopy guide`: interactive-vortex-spectroscopy.html

.. figure:: /_static/images/guides/vortex_core_spectroscopy.png

    Figure: Vortex spectra in *s*-wave and *d*-wave superconductors. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

----


|start-h2| Mesoscopic vortex lattices |end-h2|

This guide shows how to study the arrangement of vortices in mesoscopic superconducting grains.

`Click here to go to the guide on mesoscopic vortex lattices`_, to produce the following figure:

   .. _`Click here to go to the guide on mesoscopic vortex lattices`: mesoscopic-vortex-lattices.html

..
    .. figure:: /_static/images/guides/mesoscopic_vortex_lattices.png

    Figure: Vortex-lattices influenced by shape and size of mesoscopic grains. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.


----


|start-h2| Zero-energy Andreev bound states |end-h2|

This guide shows how to study zero-energy Andreev bound states at the edges of unconventional *d*-wave superconductors, and how the states are Doppler-shifted to finite energies by external fields.

`Click here to go to the guide on Zero-energy Andreev bound states`_, to produce the following figures:

   .. _`Click here to go to the guide on Zero-energy Andreev bound states`: abs-doppler-shift.html

.. figure:: /_static/images/guides/abs_doppler_shift.png

    Figure: Zero-energy Andreev bound states at the pairbreaking [110]-edges of a *d*-wave superconductor, exposed to external magnetic fields. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

.. figure:: ../_static/images/guides/abs_doppler_shift_interactive.png

    Figure: Screenshot of the interactive spectroscopy tool, used to study the a *d*-wave superconductor, where zero-energy Andreev bound states are Doppler shifted due to external magnetic fields. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

----


|start-h2| Studying phase crystals |end-h2|

This guide shows how to study the phenomenon of "phase crystallization" in unconventional *d*-wave superconductors, shaped like octagons and discs.

`Click here to go to the guide on phase crystals`_, to produce the following figures:

   .. _`Click here to go to the guide on phase crystals`: phase-crystals.html

.. figure:: /_static/images/guides/phase_crystal.png

    Figure: Phase crystallization at the pairbreaking [110]-edges of a *d*-wave superconductor. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

.. figure:: /_static/images/guides/phase_crystal_interactive.png

    Figure: Screenshot of the interactive spectroscopy tool, used to study phase crystallization at the pairbreaking [110]-edges of a *d*-wave superconductor. See the corresponding guide for analysis and a step-by-step guide how to reproduce the figure using SuperConga.

.. |start-h2| raw:: html

     <h2>

.. |end-h2| raw:: html

     </h2>